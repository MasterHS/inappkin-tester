//
//  ADSServerRequestHandler.h
//  iNappkin Test
//
//  Created by Fei Pan on 27/05/14.
//  Copyright (c) 2014 LS1 TUM. All rights reserved.
//

@import Foundation;

@class ADSProject;
@class ADSSketch;
@class ADSUIElement;
@class ADSTask;

@interface ADSServerRequestHandler : NSObject

/**
 * Static project upload
 */
+ (void) uploadProject : (ADSProject *) project serverRequestHandler : (ADSServerRequestHandler *)
	serverRequestHandler;

/**
 * Initialization
 *
 * Example Usage:
 * @code
 *
 * NSString *  baseURLStringForGeneralFunctionality = @"http://ios14adesso-bruegge.in.tum.de:7000/";
 * NSString * baseURLStringForImageDownload = @"http://ios14adesso-bruegge.in.tum.de:7001/";
 *
 * self.requestHandler = [[ADSServerRequestHandler alloc] initWithBaseURLStringForGeneralFunctionality:baseURLStringForGeneralFunctionality baseURLStringForImageDownload:baseURLStringForImageDownload];
 * @endcode
 *
 * @param baseURLStringForGeneralFunctionality
 *  The full HTTP path for REST,GET,PUT,POST to get the project info and upload images/files.
 * @param baseURLStringForImageDownload
 *  The full HTTP path for download images
 *
 */
- (id)                    initWithBaseURLStringForGeneralFunctionality : (NSString *)
	baseURLStringForGeneralFunctionality baseURLStringForImageDownload : (NSString *)
	baseURLStringForImageDownload;


/**
 * Download all projects information (including sketches, uielement, tasks...) via JSON from server based on HTTP GET. This is an asynchronize function using blocks.
 *  1. Success
 *  2. failure
 *
 * Example Usages:
 * @code
 *
 * [self.requestHandler downloadAllProjectsInformationToProjectArraySuccess:^(NSArray *returnProjects) {
 *  [self.projectArray addObjectsFromArray:returnProjects];
 *  [self.projectCollectionView reloadData];
 * } failure:^(NSError *error) {
 *  NSLog(@"Error to get %@!", error);
 * }];
 *
 * @endcode
 *
 * @param returnProjects
 *  A NSArray stores the return array of all projects from server database. Called in success block when the server successfully get the data.
 * @param error
 *  The error if download is NOT successful. Called in failure block.
 *
 */

- (void)
	downloadAllProjectsInformationToProjectArraySuccess : ( void (^) (NSArray *
																	  returnProjects) ) sucess
												failure : ( void (^) (NSError * error) )failure;

/**
 * Download all projects information via JSON from server based on HTTP GET. This is an asynchronize function using blocks.
 *  1. Success
 *  2. failure
 *
 * Example Usages:
 * @code
 *
 * [self.requestHandler downloadProjectArraySuccess:^(NSArray *returnProjects) {
 *  [self.projectArray addObjectsFromArray:returnProjects];
 *  [self.projectCollectionView reloadData];
 * } failure:^(NSError *error) {
 *  NSLog(@"Error to get %@!", error);
 * }];
 *
 * @endcode
 *
 * @param returnProjects
 *  A NSArray stores the return array of all projects from server database. Called in success block when the server successfully get the data.
 * @param error
 *  The error if download is NOT successful. Called in failure block.
 *
 */

- (void)
	downloadProjectArraySuccess : ( void (^
										  ) (NSArray
											 *
											 returnProjects) )
	sucess              failure : ( void (^
										  ) (
										NSError * error) )failure;

/**
 * Create one project information via JSON to server based on HTTP POST. This is an asynchronize function using blocks.
 * 1. success
 * 2. failure
 *
 * Example Usages:
 * @code
 *
 * NTProject * postProject = self.projectArray[0];
 *
 * [self.requestHandler createProject:postProject success:^{
 *  NSLog(@"Sucess!");
 * } failure:^(NSError *error) {
 *  NSLog(@"Error to create %@!", error);
 * }];
 *
 * @endcode
 *
 * @param project
 *  The error if download is NOT successful. Called in failure block.
 *
 */
- (void)
							  createProject : (
		ADSProject *) project
									success : ( void (^
					  ) (
					void) ) success failure : (
		void (
			^
			) (
			NSError
			*
			error) )
	failure;


/**
 * Update one project information via JSON to server based on HTTP PUT. This is an asynchronize function using blocks.
 * 1. success
 * 2. failure
 *
 * Example Usages:
 * @code
 *
 * NTProject * postProject = self.projectArray[0];
 *
 * [self.requestHandler updateProject:project success:^{
 *  NSLog(@"Sucess!");
 * } failure:^(NSError *error) {
 *  NSLog(@"Error to update %@!", error);
 * }];
 *
 * @endcode
 *
 * @param project
 *  The error if download is NOT successful. Called in failure block.
 */
- (void)
							  updateProject : (
		ADSProject *) project
									success : ( void (^
					  ) (
					void) ) success failure : (
		void (
			^
			) (
			NSError
			*
			error) )
	failure;


/**
 * Delete one project information via JSON to server based on HTTP DELETE. This is an asynchronize function using blocks.
 * 1. success
 * 2. failure
 *
 * Example Usages:
 * @code
 *
 * NTProject * postProject = self.projectArray[0];
 *
 * [self.requestHandler deleteProject:project success:^{
 *  NSLog(@"Sucess!");
 * } failure:^(NSError *error) {
 *  NSLog(@"Error to delete %@!", error);
 * }];
 *
 * @endcode
 *
 * @param project
 *  The error if download is NOT successful. Called in failure block.
 */
- (void)
							  deleteProject : (
		ADSProject *) project
									success : ( void (^
					  ) (
					void) ) success failure : (
		void (
			^
			) (
			NSError
			*
			error) )
	failure;


/**
 * Upload one image file of one project via JSON to server based on HTTP POST Multifile. This is an asynchronize function using blocks.
 * 1. success
 * 2. failure
 *
 * Example Usages:
 * @code
 *
 * NTProject * project = self.projectArray[0];
 * NSString * filePath = [[NSBundle mainBundle] pathForResource:@"guy" ofType:@"jpg"];
 * NSData * imageData = [NSData dataWithContentsOfFile:filePath];
 *
 * [self.requestHandler uploadImageData:imageData fileName:@"guy" project:project success:^{
 *  NSLog(@"Sucess!");
 * } failure:^(NSError *error) {
 *  NSLog(@"Error to Upload Image %@!", error);
 * }];
 *
 * @endcode
 *
 * @param imageData
 *  The data of image file we want to upload to the server
 * @param fileName
 *  The file name of the file we want to save on the server. This parameter is not in use now.
 * @param project
 *  The project we want to use to update on the server.
 * @param error
 *  The error if download is NOT successful. Called in failure block.
 */
- (void) uploadZipPath : (NSString *) zipPath fileName : (NSString *) fileName project : (ADSProject
																						  * )
	project    success : ( void (^) (void) ) success failure : ( void (^) (NSError *
																		   error) )failure;

/**
 * Download one image file of one project via JSON to server based on HTTP Download Request. This is an asynchronize function using blocks.
 * 1. success
 * 2. failure
 *
 * Example Usages:
 * @code
 *
 * NTProject * project = self.projectArray[3];
 *
 * [self.requestHandler downloadSketchImageFilesWithProject: project success:^(NSURL *responseFileURL) {
 *  NSLog(@"File download to %@", responseFileURL.path);
 *  self.testImage.image = [[UIImage alloc] initWithContentsOfFile:responseFileURL.path];
 * } failure:^(NSError *error) {
 *  NSLog(@"Error to download Image %@!", error);
 * }];
 *
 * @endcode
 *
 * @param fileName
 *  The file name of the file we want to download from the server. This parameter is not in use now.
 * @param project
 *  The project we want to use to download on the server.
 * @param responseFileURL
 *  The URL of file which is downloaded successfully and saved on the LOCAL device. Called in success block
 * @param error
 *  The error if download is NOT successful. Called in failure block.
 */
- (void)
	downloadSketchImageFilesWithProject : (ADSProject *) project success : ( void (^) (NSURL *
																					   responseFileURL) )
	success                     failure : ( void (^) (NSError * error) )failure;


- (void)
	downloadSketchArrayWithProject : (ADSProject *) project Success : ( void (^) (NSArray *
																				  returnSketches) )
	sucess                 failure : ( void (^) (NSError * error) )failure;

- (void)
	createSketch
			: (ADSSketch *) sketch project : (ADSProject *) project success : ( void (^
																			  ) (
																			void) ) success
	failure : ( void (^) (NSError * error) )failure;

- (void)
	updateSketch
			: (ADSSketch *) sketch project : (ADSProject *) project success : ( void (^
																			  ) (
																			void) ) success
	failure : ( void (^) (NSError * error) )failure;

- (void)
	deleteSketch
			: (ADSSketch *) sketch project : (ADSProject *) project success : ( void (^
																			  ) (
																			void) ) success
	failure : ( void (^) (NSError * error) )failure;

- (void)
	downloadUIElementArrayWithProject : (ADSProject
										 *) project
						 parentSketch : (ADSSketch *)
	sketch
							  Success : ( void (^
					  ) (
					NSArray *
					returnSketches) )
	sucess                    failure : ( void (^
												) (
											  NSError *
											  error) )
	failure;


- (void)
							createUIElement : (ADSUIElement
					   *) UIElement
							   parentSketch : (ADSSketch *) sketch
	parentproject
											: (ADSProject *)
	project
									success : ( void (^
					  ) (
					void) ) success failure : ( void (^
													  ) (
													NSError
													*
													error) )
	failure;

- (void)                         updateUIElement : (ADSUIElement *) UIElement
									parentSketch : (ADSSketch *) sketch parentproject
												 : (ADSProject *) project
										 success : ( void (^
					  ) (void) ) success failure : ( void (^) (NSError *
															   error) )failure;

- (void)
							deleteUIElement : (ADSUIElement
					   *) UIElement
							   parentSketch : (ADSSketch *) sketch
	parentproject
											: (ADSProject *)
	project
									success : ( void (^
					  ) (
					void) ) success failure : ( void (^
													  ) (
													NSError
													*
													error) )
	failure;

- (void)
	downloadTaskArrayWithProject : (ADSProject *) project Success : ( void (^) (NSArray *
																				returnTasks) )
	sucess               failure : ( void (^) (NSError * error) )failure;

- (void)
	createTask
			: (ADSTask *) newTask project : (ADSProject *) project success : ( void (^
																			 ) (
																		   void) ) success
	failure : ( void (^) (NSError * error) )failure;

- (void)
	updateTask
			: (ADSTask *) aTask project : (ADSProject *) project success : ( void (^
																		   ) (
																		 void) ) success
	failure : ( void (^) (NSError * error) )failure;

- (void)
	deleteTask
			: (ADSTask *) aTask project : (ADSProject *) project success : ( void (^
																		   ) (
																		 void) ) success
	failure : ( void (^) (NSError * error) )failure;

- (void)                      downloadActionArrayWithTask : (ADSTask *) aTask parentProject : (
		ADSProject *) project
												  Success : ( void (^) (NSArray *
						  returnActions) ) sucess failure : ( void (^
																	) (NSError *
																	   error) )failure;

- (void)
	updateActionsWithTask
					: (ADSTask * ) aTask project : (ADSProject *) project success : ( void (^
																			) (
																		  void) )
	success failure : ( void (^) (NSError * error) )failure;

- (void)                    createTap : (CGPoint) tap task : (ADSTask *) aTask sketch : (ADSSketch *)
	sketch                    project : (
		ADSProject *) project success : ( void (^) (void) ) success failure : ( void (^) (NSError *
																						  error) )
	failure;

- (void) createTestResult : (NSString *) result task : (ADSTask *) aTask project : (ADSProject *)
	project       success : ( void (^) (void) ) success failure : ( void (^) (NSError *
																			  error) )failure;

@end
