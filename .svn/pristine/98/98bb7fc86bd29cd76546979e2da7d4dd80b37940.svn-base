//
//  CoreDataManager.h
//  didacta
//
//  Created by Avr Messe on 21.06.10.
//  Copyright 2010 AVR GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Project, Sketch,TransitionButton;


@interface CoreDataManager : NSObject {
	
	/*
	 Class for data management
	 */
	
	@private
    NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;	    
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
}

@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (CoreDataManager *)sharedManager;

- (NSError *)saveData;

-(Project*) createProjectWithName:(NSString*)name description:(NSString*)desc deviceType:(NSString*)deviceType;
-(Project*) createProjectWithDataDictionary:(NSDictionary*)dict;

-(Sketch *)createSketchWithImageFilePath:(NSString*)path imageFile:(NSData*)file;
-(Sketch *)createSketchWithDataDictionary:(NSDictionary*)dict;

-(TransitionButton*)createTransitionButtonWithRect:(CGRect)buttonRect;
-(TransitionButton*)createTransitionButtonWithDictionary:(NSDictionary*)dict;

-(void)deleteProject:(Project*)project;
-(void)deleteSketch:(Sketch*)sketch fromProject:(Project*)project;
-(void)deleteTransitionButton:(TransitionButton*)button fromSketch:(Sketch*)sketch;

/*
 Creates and returns a ManagedObject with name entityName
 
 @param entityName the name for the new ManagedObject
 
 @return returns a newly created ManagedObject  
 
 */

- (void)resetDatastore;

@end
