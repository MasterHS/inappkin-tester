//
//  NTMenuViewControlleriPad.m
//  iNappkinTester
//
//  Created by Fei Pan on 21/05/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NTProjectsViewController.h"
#import "ADSProject.h"
#import "ADSSketch.h"
#import "ADSStoryboard.h"
#import "NTTasksViewController.h"
#import "ADSServerRequestHandler.h"

#import "NTProjectCollectionViewCell.h"

#import "CoreDataManager.h"
#import <CoreData/CoreData.h>
#import "SlimServerRequestHandler.h"
#import "NTHelpers.h"
#import "Project.h"
#import "Sketch.h"
#import "NTProjectViewController.h"

@interface NTProjectsViewController ()

@end

@implementation NTProjectsViewController

#pragma mark - private methods

- (void) setShowShakeDefaults{
	NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
	if ([defaults objectForKey : @"showShake"] == nil) {
		[defaults setBool : YES forKey : @"showShake"];
		[defaults synchronize];
	}
}

#pragma mark - view controller methods
- (void) viewDidLoad{
	[super viewDidLoad];
	[self getAllProjects];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeUpdateView) name:@"ServercommunicationEnded" object:nil];
    
	[self setShowShakeDefaults];
}

- (void) viewDidAppear : (BOOL) animated{
	[super viewDidAppear : animated];
	[self.projectsCollectionView reloadData];
}


#pragma mark - UICollectionView Data Source
- (NSInteger) numberOfSectionsInCollectionView : (UICollectionView *) collectionView{
	return 1;
}

- (NSInteger) collectionView : (UICollectionView *) collectionView numberOfItemsInSection : (NSInteger)section{
	return [self.projectArray count];
}

- (UICollectionViewCell *) collectionView : (UICollectionView *) collectionView cellForItemAtIndexPath: (NSIndexPath *) indexPath{
	NTProjectCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"NTMainMenuCollectionViewCell" forIndexPath : indexPath];
    
    Project *project= [self.projectArray objectAtIndex:indexPath.row];
    
    //Cell setup
	cell.projectNameLabel.text = project.name;
	cell.projectNameLabel.numberOfLines = 0; // allow line breaks
	cell.projectNameLabel.textAlignment = NSTextAlignmentCenter; // center align
    
	// Project thumbnail
	UIImage * image;
    
    if ([[project sketches]count]> 0) {
        Sketch *tSketch = [[project sketches]objectAtIndex:0];
        image= [UIImage imageWithData:tSketch.imageFile];
    }
    else image = [UIImage imageNamed : @"ProjectDummyThumbnail"];
    cell.projectThumbnail.image = image;
    
	return cell;
}

#pragma mark - private methods
- (IBAction)loadProjectsFromServer:(id)sender {
    [[CoreDataManager sharedManager] resetDatastore];
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    self.updateIndicatorView.hidden= NO;
    [self.view bringSubviewToFront:self.updateIndicatorView];
    
    SlimServerRequestHandler *requestHandler= [[SlimServerRequestHandler alloc] initWithBaseURLStringForGeneralFunctionality:[NTHelpers getBaseServerName] baseURLStringForImageDownload:[NTHelpers getImageServerName]];
    
    [requestHandler downloadAllProjectsWithAllSubdata];
    
    [self getAllProjects];
    [self.projectsCollectionView reloadData];
}

#pragma mark - segue
- (void) prepareForSegue : (UIStoryboardSegue *) segue sender : (id) sender{
	if ([sender isKindOfClass : [UICollectionViewCell class]]) {
		NSIndexPath * indexPath = [self.projectsCollectionView indexPathForCell : sender];
		if (indexPath) {
			if ([segue.identifier isEqualToString : @"OpenProject"]) {
                NTProjectViewController *aVC = segue.destinationViewController;
                Project * selectedProject = self.projectArray[indexPath.row];
                aVC.currentProject = selectedProject;
			}
		}
	}
}

#pragma mark - Notification Methoden
-(void)removeUpdateView{
    [self getAllProjects];
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    self.updateIndicatorView.hidden = YES;
    [[CoreDataManager sharedManager] saveData];
}


#pragma mark - Datenbankaufrufe
-(void)getAllProjects{
    NSManagedObjectContext *managedObjectContext = [[CoreDataManager sharedManager] managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:managedObjectContext];
	[fetchRequest setEntity:entity];
	
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects: sortDescriptor, nil];
   	[fetchRequest setSortDescriptors:sortDescriptors];
    
	NSFetchedResultsController *fetchedExhibitorController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
	[fetchedExhibitorController performFetch:nil];
    self.projectArray = [fetchedExhibitorController fetchedObjects];
    [self.projectsCollectionView reloadData];
}

@end
