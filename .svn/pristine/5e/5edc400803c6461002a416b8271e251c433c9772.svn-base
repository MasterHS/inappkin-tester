//
//  NTTaskController.m
//  iNappkinTester
//
//  Created by Dan on 22/6/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NTTaskViewController.h"
#import "ADSTask.h"
#import "ADSSketch.h"
#import "ADSButton.h"
#import "ADSTestSuite.h"
#import "ADSTransition.h"
#import "ADSTapEvent.h"
#import "NTHelpers.h"

@interface NTTaskViewController ()

@end

@implementation NTTaskViewController

#pragma mark- private methods
- (UIImageView *) imageView
{
	if (!_imageView) {
		_imageView = [[UIImageView alloc]init];
	}
	return _imageView;
}

#pragma  mark - view controller methods
- (id) initWithNibName : (NSString *) nibNameOrNil bundle : (NSBundle *) nibBundleOrNil
{
	self = [super initWithNibName : nibNameOrNil bundle : nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void) viewDidLoad
{
	[super viewDidLoad];
	self.navigationItem.title = self.task.name;
	self.navigationController.navigationBarHidden = NO;
	self.imageView.image = self.task.firstSketch.image;
	self.sketch = self.task.firstSketch;
}

- (IBAction) doneButtonClicked : (id) sender
{

}

- (IBAction) tap : (UITapGestureRecognizer *) sender
{
	[self.navigationController setNavigationBarHidden : YES animated : YES];

	CGPoint tapPoint = [sender locationInView : self.imageView];
	NSTimeInterval timestamp = [[NSDate date] timeIntervalSince1970];

	// collect data
	[self.task.testSuite addTapEvent : [[ADSTapEvent alloc]initWithLocation : tapPoint onSketch : self.
										sketch                       atTime : timestamp]];
	// change the sketch if it is tapped inside a defined button area
	for (int i = 0; i < [self.sketch elementsCount]; i++) {
		if ([self.sketch.elements[i] isMemberOfClass : [ADSButton class]]) {
			ADSButton * button = self.sketch.elements[i];

			// resize button.element from 480x640
			NSString * currentDevice = [NTHelpers currentDevice];
			CGRect location = button.location;
			if ([currentDevice isEqualToString : @"iPad Retina"]) {
				// to 768x1024: 1.6x1.6
				location.size.width = 1.6 * button.location.size.width;
				location.size.height = 1.6 * button.location.size.height;
				location.origin.x = 1.6 * button.location.origin.x;
				location.origin.y = 1.6 * button.location.origin.y;
			} else if ([currentDevice isEqualToString : @"iPhone 5"]) {
				// to 320x568: 0.66x0.8875
				// but use only 0.8875 for now due to the constant aspect ratio in Designer
				location.size.width = 0.8875 * button.location.size.width;
				location.size.height = 0.8875 * button.location.size.height;
				location.origin.x = 0.8875 * button.location.origin.x - 53; // hide the margin
				location.origin.y = 0.8875 * button.location.origin.y;
			} else if ([currentDevice isEqualToString : @"iPhone 4"]) {
				// to 320x480: 0.66x0.75
				// but use only 0.75 for now due to the constant aspect ratio in Designer
				location.size.width = 0.75 * button.location.size.width;
				location.size.height = 0.75 * button.location.size.height;
				location.origin.x = 0.75 * button.location.origin.x - 20; // hide the margin
				location.origin.y = 0.75 * button.location.origin.y;
			} else {
				// assume iPad compatibility: 768x1024: 1.6x1.6
				location.size.width = 1.6 * button.location.size.width;
				location.size.height = 1.6 * button.location.size.height;
				location.origin.x = 1.6 * button.location.origin.x;
				location.origin.y = 1.6 * button.location.origin.y;
			}

			if ( CGRectContainsPoint(location, tapPoint) ) {
				if (button.transition.destinationSketch != nil) {
					self.sketch = button.transition.destinationSketch;

					// display sketch with animation
					self.imageView.image = self.sketch.image;
					CATransition * transition = [CATransition animation];
					transition.duration = 0.3f;
					transition.timingFunction =
						[CAMediaTimingFunction functionWithName : kCAMediaTimingFunctionLinear];
					transition.type = kCATransitionFromLeft;

					[self.imageView.layer addAnimation : transition forKey : nil];
				}
			}
		}
	}

}

- (BOOL) canBecomeFirstResponder
{
	return YES;
}

- (void) motionEnded : (UIEventSubtype) motion withEvent : (UIEvent *) event
{
	if (motion == UIEventSubtypeMotionShake) {
		[self.navigationController setNavigationBarHidden : NO animated : YES];
	}
}

/*
   #pragma mark - Navigation

   // In a storyboard-based application, you will often want to do a little preparation before navigation
   - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
   {
   // Get the new view controller using [segue destinationViewController].
   // Pass the selected object to the new view controller.
   }
 */

@end
