#!/bin/sh

# This script formats files in parameter given sources folder
# Usage : $ sh CodeFormatter/formatAllSources.sh /Main/Classes

# SETTINGS
# set path to uncrustify binary
# UNCRUSTIFYLOCATION="/usr/bin/uncrustify"
UNCRUSTIFYLOCATION=/opt/local/bin/uncrustify

if test -e $UNCRUSTIFYLOCATION; then
	if [ -n "$1" ]
		then
		
		# recover directory to format :
		pathToSourcesDirectory=`echo $(pwd)/$1`
		
		# go to current folder :
		scriptDirectory=$(dirname $0)
		cd $scriptDirectory
		
		# find sources files to format :
		echo ""
		echo "info: ==> Getting files to format in directory " + $pathToSourcesDirectory
		mkdir -p temp
		find "$pathToSourcesDirectory" -name "*.[mh]" -not -path "*/iNappkinTester/Libraries/Network/*AFNetworking/*" > temp/sources_to_uncrustify.txt
		
		# format files :
		echo ""
		echo "==> Format files"	
		$UNCRUSTIFYLOCATION -F temp/sources_to_uncrustify.txt -c "./uncrustify.cfg" --no-backup 
		
		# remove temp files : 
		rm -rf temp/
		
	else 
		echo "Error : You must specify a source folder as first parameter"	
	
	fi
else
	echo "warning: Command uncrustify not found"
fi	
