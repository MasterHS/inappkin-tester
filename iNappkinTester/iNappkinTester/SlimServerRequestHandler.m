//
//  SlimServerRequestHandler.m
//  iNappkinDesigner
//
//  Created by Julian-Lennart Weigand on 19.08.14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "SlimServerRequestHandler.h"
#import "Project.h"
#import "Sketch.h"
#import "TransitionButton.h"
#import "CoreDataManager.h"
#import "NTHelpers.h"
#import "TaskStat.h"
#import "Task.h"

@implementation SlimServerRequestHandler



- (id)initWithBaseURLStringForGeneralFunctionality: (NSString *)baseURLStringForGeneralFunctionality{
    self = [super init];
    
    if (self) {
        self.baseURLStringForGeneralManager = baseURLStringForGeneralFunctionality;
        NSURL * baseURLForGeneralManager =[[NSURL alloc] initWithString : self.baseURLStringForGeneralManager];
        self.generalManager = [[AFHTTPSessionManager alloc] initWithBaseURL: baseURLForGeneralManager];
    }
    return self;
}

#pragma mark - Test Server URL
- (void) testServerURL{
    AFHTTPSessionManager * generalManager = self.generalManager;
    generalManager.requestSerializer = [AFJSONRequestSerializer serializer];
    generalManager.responseSerializer = [AFJSONResponseSerializer serializer];
    NSDictionary * projectDict = @{
                                   @"projectName" : @"TESTNAME",
                                   @"projectDescription" : @"TESTDESCR",
                                   @"deviceId" : @"TESTDEVICE",
                                   @"deviceType" : @"TESTDEVICETYPE",
                                   };
    
    [generalManager POST : @"project" parameters : projectDict success :^ (NSURLSessionDataTask *task,id responseObject) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"SUCCESS" message:@"Server URL is valid." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
        
        NSDictionary * dictionary = (NSDictionary *) responseObject;
        [generalManager DELETE:[NSString stringWithFormat : @"project/%@", [dictionary objectForKey:@"_id"]] parameters: nil success :^ (NSURLSessionDataTask * task,id responseObject) {
        } failure :^ (NSURLSessionDataTask * task, NSError * error) {
        }];
    } failure :^ (NSURLSessionDataTask * task, NSError * error) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"ERROR" message:@"Server URL is invalid." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
    }];
}

-(void)startSyncingProcess
{
    /*
     Synchronization is a single request. The request contains:
     -The MAX last_modified time stamp of your model objects. This tells the server you only want changes after this time stamp.
     -A JSON array containing all items with sync_status=1.
     */
    //NSString *project_lastModified=@"";//[NDHelpers getLastModified];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    NSLocale* posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormat.locale = posix;
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"CET"]];
    [self syncProjects:dateFormat];
}

-(void)syncProjects:(NSDateFormatter*)dateFormat
{
    AFHTTPSessionManager * generalManager = self.generalManager;
    generalManager.requestSerializer=[AFJSONRequestSerializer serializer];
    generalManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSArray* projectsToSync=[[CoreDataManager sharedManager]DataToSync:@"Project"];
    {
        int counter=0;
        NSMutableDictionary* br=[[NSMutableDictionary alloc]init];
        Project* maxLastModifiedProject=[[CoreDataManager sharedManager]getMaxLastModifiedinTable:@"Project"];
        if(maxLastModifiedProject==nil)
            [br setObject:@"" forKey:@"MaxLastModified"];
        else
        {
            if (maxLastModifiedProject.last_modified==nil) {
                maxLastModifiedProject.last_modified=[dateFormat dateFromString:@"1970-0-01T00:00:00:000Z"];
            }
            else
                [br setObject:[dateFormat stringFromDate:maxLastModifiedProject.last_modified] forKey:@"MaxLastModified"];
        }
        for (NSMutableDictionary* tempDict in projectsToSync) {
            NSString * dateToString=[dateFormat stringFromDate:[tempDict objectForKey:@"last_modified"]];
            [tempDict setObject:dateToString forKey:@"last_modified"];
            [br setObject:tempDict forKey:[NSString stringWithFormat:@"object %i",counter]];
            counter++;
        }
        
        [generalManager POST: @"project" parameters : br success :^ (NSURLSessionDataTask * task,id responseObject) {
            NSLog(@"1:6 Projects Received");
            for (NSDictionary * projectDictionary in responseObject) {
                if([[projectDictionary objectForKey:@"is_deleted"] isEqualToString:@"0"])
                    [[CoreDataManager sharedManager]createOrModifyProject:projectDictionary projectGuid:[projectDictionary objectForKey:@"guid"] syncStatus:@"0" deleted:@"0"];
                else//put deleted to 2 => delted project on client side permenantly
                    [[CoreDataManager sharedManager]createOrModifyProject:projectDictionary projectGuid:[projectDictionary objectForKey:@"guid"] syncStatus:@"0" deleted:@"2"];
            }
            [NTHelpers setSyncStatus:@"1"];
            [self syncSketches:dateFormat];
        }
                    failure :^ (NSURLSessionDataTask * sessionTask, NSError * error) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ProjectsFinshedSyncing" object:nil];
                        NSLog(@"%@",[error localizedDescription]);
                    }];
    }
    
    
}

-(void)syncSketches:(NSDateFormatter*)dateFormat
{
    AFHTTPSessionManager * generalManager = self.generalManager;
    generalManager.requestSerializer=[AFJSONRequestSerializer serializer];
    generalManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSArray* sketchesToSync=[[CoreDataManager sharedManager]DataToSync:@"Sketch"];
    {
        int counter=0;
        NSMutableDictionary* br=[[NSMutableDictionary alloc]init];
        Sketch* maxLastModifiedSketch=[[CoreDataManager sharedManager]getMaxLastModifiedinTable:@"Sketch"];
        if(maxLastModifiedSketch==nil)
            [br setObject:@"" forKey:@"MaxLastModified"];
        else
        {
            if (maxLastModifiedSketch.last_modified==nil) {
                maxLastModifiedSketch.last_modified=[dateFormat dateFromString:@"1970-0-01T00:00:00:000Z"];
            }
            else
                [br setObject:[dateFormat stringFromDate:maxLastModifiedSketch.last_modified] forKey:@"MaxLastModified"];
        }
        for (NSMutableDictionary* tempDict in sketchesToSync) {
            NSString * dateToString=[dateFormat stringFromDate:[tempDict objectForKey:@"last_modified"]];
            [tempDict setObject:dateToString forKey:@"last_modified"];
            
            NSString *base64String = [[tempDict objectForKey:@"imageFile"]  base64EncodedStringWithOptions:0];
            if(base64String==nil)
                [tempDict setObject:@"" forKey:@"imageFile"];
            else
                [tempDict setObject:base64String forKey:@"imageFile"];
            
            [br setObject:tempDict forKey:[NSString stringWithFormat:@"object %i",counter]];
            counter++;
        }
        
        [generalManager POST: @"sketch" parameters : br success :^ (NSURLSessionDataTask * task,id responseObject) {
            NSLog(@"2:6 Sketches Received");
            for (NSDictionary * sketchDictionary in responseObject) {
                if([[sketchDictionary objectForKey:@"is_deleted"] isEqualToString:@"0"])
                    [[CoreDataManager sharedManager]createOrModifySketch:sketchDictionary sketchGuid:[sketchDictionary objectForKey:@"guid"] projectGuid:[sketchDictionary objectForKey:@"projectGuid"] syncStatus:@"0" deleted:@"0"];
                else//deleted ==2 => delted project on client side permenantly
                    [[CoreDataManager sharedManager]createOrModifySketch:sketchDictionary sketchGuid:[sketchDictionary objectForKey:@"guid"] projectGuid:[sketchDictionary objectForKey:@"projectGuid"] syncStatus:@"0" deleted:@"2"];
            }
            [NTHelpers setSyncStatus:@"1"];
            [self syncTransitionButtons:dateFormat];
        }
                    failure :^ (NSURLSessionDataTask * sessionTask, NSError * error) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ProjectsFinshedSyncing" object:nil];
                        NSLog(@"%@",[error localizedDescription]);
                    }];
    }
    
    
}

-(void)syncTransitionButtons:(NSDateFormatter*)dateFormat
{
    AFHTTPSessionManager * generalManager = self.generalManager;
    generalManager.requestSerializer=[AFJSONRequestSerializer serializer];
    generalManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSArray* buttonsToSync=[[CoreDataManager sharedManager]DataToSync:@"TransitionButton"];
    {
        int counter=0;
        NSMutableDictionary* br=[[NSMutableDictionary alloc]init];
        TransitionButton* maxLastModifiedButton=[[CoreDataManager sharedManager]getMaxLastModifiedinTable:@"TransitionButton"];
        if(maxLastModifiedButton==nil)
            [br setObject:@"" forKey:@"MaxLastModified"];
        else
        {
            if (maxLastModifiedButton.last_modified==nil) {
                maxLastModifiedButton.last_modified=[dateFormat dateFromString:@"1970-0-01T00:00:00:000Z"];
            }
            else
                [br setObject:[dateFormat stringFromDate:maxLastModifiedButton.last_modified] forKey:@"MaxLastModified"];
        }
        for (NSMutableDictionary* tempDict in buttonsToSync) {
            NSString * dateToString=[dateFormat stringFromDate:[tempDict objectForKey:@"last_modified"]];
            [tempDict setObject:dateToString forKey:@"last_modified"];
            [br setObject:tempDict forKey:[NSString stringWithFormat:@"object %i",counter]];
            counter++;
        }
        
        [generalManager POST: @"transitionbutton" parameters : br success :^ (NSURLSessionDataTask * task,id responseObject) {
            NSLog(@"3:6 TransitionButtons Received");
            for (NSDictionary * buttonDictionary in responseObject) {
                if([[buttonDictionary objectForKey:@"is_deleted"] isEqualToString:@"0"])
                    
                    [[CoreDataManager sharedManager]createOrModifyTransitionButton:buttonDictionary transButtonGuid:[buttonDictionary objectForKey:@"guid"] sketchGuid:[buttonDictionary objectForKey:@"sketchGuid"] syncStatus:@"0" deleted:@"0"];
                else//deleted ==2 => delted project on client side permenantly
                    [[CoreDataManager sharedManager]createOrModifyTransitionButton:buttonDictionary transButtonGuid:[buttonDictionary objectForKey:@"guid"] sketchGuid:[buttonDictionary objectForKey:@"sketchGuid"] syncStatus:@"0" deleted:@"2"];
            }
            [self syncTasks:dateFormat];
            
        }
                    failure :^ (NSURLSessionDataTask * sessionTask, NSError * error) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ProjectsFinshedSyncing" object:nil];
                        NSLog(@"%@",[error localizedDescription]);
                    }];
    }
    
    
}


-(void)syncTasks:(NSDateFormatter*)dateFormat
{
    AFHTTPSessionManager * generalManager = self.generalManager;
    generalManager.requestSerializer=[AFJSONRequestSerializer serializer];
    generalManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSArray* tasksToSync=[[CoreDataManager sharedManager]DataToSync:@"Task"];
    {
        int counter=0;
        NSMutableDictionary* br=[[NSMutableDictionary alloc]init];
        Task* maxLastModifiedTask=[[CoreDataManager sharedManager]getMaxLastModifiedinTable:@"Task"];
        if(maxLastModifiedTask==nil)
            [br setObject:@"" forKey:@"MaxLastModified"];
        else
        {
            if (maxLastModifiedTask.last_modified==nil) {
                maxLastModifiedTask.last_modified=[dateFormat dateFromString:@"1970-0-01T00:00:00:000Z"];
            }
            else
                [br setObject:[dateFormat stringFromDate:maxLastModifiedTask.last_modified] forKey:@"MaxLastModified"];
        }
        for (NSMutableDictionary* tempDict in tasksToSync) {
            NSString * dateToString=[dateFormat stringFromDate:[tempDict objectForKey:@"last_modified"]];
            [tempDict setObject:dateToString forKey:@"last_modified"];
            [br setObject:tempDict forKey:[NSString stringWithFormat:@"object %i",counter]];
            counter++;
        }
        
        [generalManager POST: @"task" parameters : br success :^ (NSURLSessionDataTask * task,id responseObject) {
            NSLog(@"4:6 Tasks Received");
            for (NSDictionary * taskDictionary in responseObject) {
                if([[taskDictionary objectForKey:@"is_deleted"] isEqualToString:@"0"])
                    [[CoreDataManager sharedManager]createOrModifyTask:taskDictionary taskGuid:[taskDictionary objectForKey:@"guid"]  projectGuid:[taskDictionary objectForKey:@"projectGuid"] syncStatus:@"0" deleted:@"0"];
                else//deleted ==2 => delted project on client side permenantly
                    [[CoreDataManager sharedManager]createOrModifyTask:taskDictionary taskGuid:[taskDictionary objectForKey:@"guid"]  projectGuid:[taskDictionary objectForKey:@"projectGuid"] syncStatus:@"0" deleted:@"2"];
            }
            [self syncTapEvents:dateFormat];
            
        }
                    failure :^ (NSURLSessionDataTask * sessionTask, NSError * error) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ProjectsFinshedSyncing" object:nil];
                        NSLog(@"%@",[error localizedDescription]);
                    }];
    }
    
    
}


-(void)syncTapEvents:(NSDateFormatter*)dateFormat
{
    AFHTTPSessionManager * generalManager = self.generalManager;
    generalManager.requestSerializer=[AFJSONRequestSerializer serializer];
    generalManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSArray* tapsToSync=[[CoreDataManager sharedManager]DataToSync:@"TapEvent"];
    {
        int counter=0;
        NSMutableDictionary* br=[[NSMutableDictionary alloc]init];
        TransitionButton* maxLastModifiedTap=[[CoreDataManager sharedManager]getMaxLastModifiedinTable:@"TapEvent"];
        if(maxLastModifiedTap==nil)
            [br setObject:@"" forKey:@"MaxLastModified"];
        else
        {
            if (maxLastModifiedTap.last_modified==nil) {
                maxLastModifiedTap.last_modified=[dateFormat dateFromString:@"1970-0-01T00:00:00:000Z"];
            }
            else
                [br setObject:[dateFormat stringFromDate:maxLastModifiedTap.last_modified] forKey:@"MaxLastModified"];
        }
        for (NSMutableDictionary* tempDict in tapsToSync) {
            NSString * dateToString=[dateFormat stringFromDate:[tempDict objectForKey:@"last_modified"]];
            [tempDict setObject:dateToString forKey:@"last_modified"];
            [br setObject:tempDict forKey:[NSString stringWithFormat:@"object %i",counter]];
            counter++;
        }
        
        [generalManager POST: @"tap" parameters : br success :^ (NSURLSessionDataTask * task,id responseObject) {
            NSLog(@"5:6 Taps Received");
            for (NSDictionary * tapDictionary in responseObject) {
                if([[tapDictionary objectForKey:@"is_deleted"] isEqualToString:@"0"])
                    [[CoreDataManager sharedManager]createOrModifyTapEvent:tapDictionary tapGuid:[tapDictionary objectForKey:@"guid"] sketchGuid:[tapDictionary objectForKey:@"sketchGuid"] taskGuid:[tapDictionary objectForKey:@"taskGuid"] syncStatus:@"0" deleted:@"0"];
                else//deleted ==2 => delted project on client side permenantly
                    [[CoreDataManager sharedManager]createOrModifyTapEvent:tapDictionary tapGuid:[tapDictionary objectForKey:@"guid"] sketchGuid:[tapDictionary objectForKey:@"sketchGuid"] taskGuid:[tapDictionary objectForKey:@"taskGuid"] syncStatus:@"0" deleted:@"2"];
            }
            [self syncTaskStats:dateFormat];
        }
                    failure :^ (NSURLSessionDataTask * sessionTask, NSError * error) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ProjectsFinshedSyncing" object:nil];
                        NSLog(@"%@",[error localizedDescription]);
                    }];
    }
    
    
}

-(void)syncTaskStats:(NSDateFormatter*)dateFormat
{
    AFHTTPSessionManager * generalManager = self.generalManager;
    generalManager.requestSerializer=[AFJSONRequestSerializer serializer];
    generalManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSArray* statsToSync=[[CoreDataManager sharedManager]DataToSync:@"TaskStat"];
    {
        int counter=0;
        NSMutableDictionary* br=[[NSMutableDictionary alloc]init];
        TransitionButton* maxLastModifiedStat=[[CoreDataManager sharedManager]getMaxLastModifiedinTable:@"TaskStat"];
        if(maxLastModifiedStat==nil)
            [br setObject:@"" forKey:@"MaxLastModified"];
        else
        {
            if (maxLastModifiedStat.last_modified==nil) {
                maxLastModifiedStat.last_modified=[dateFormat dateFromString:@"1970-0-01T00:00:00:000Z"];
            }
            else
                [br setObject:[dateFormat stringFromDate:maxLastModifiedStat.last_modified] forKey:@"MaxLastModified"];
        }
        for (NSMutableDictionary* tempDict in statsToSync) {
            NSString * dateToString=[dateFormat stringFromDate:[tempDict objectForKey:@"last_modified"]];
            [tempDict setObject:dateToString forKey:@"last_modified"];
            [br setObject:tempDict forKey:[NSString stringWithFormat:@"object %i",counter]];
            counter++;
        }
        
        [generalManager POST: @"stat" parameters : br success :^ (NSURLSessionDataTask * task,id responseObject) {
            NSLog(@"6:6 Stats Received");
            for (NSDictionary * statDictionary in responseObject) {
                if([[statDictionary objectForKey:@"is_deleted"] isEqualToString:@"0"])
                    [[CoreDataManager sharedManager]createOrModifyTaskStat:statDictionary statGuid:[statDictionary objectForKey:@"guid"] taskGuid:[statDictionary objectForKey:@"taskGuid"] syncStatus:@"0" deleted:@"0"];
                else
                    [[CoreDataManager sharedManager]createOrModifyTaskStat:statDictionary statGuid:[statDictionary objectForKey:@"guid"] taskGuid:[statDictionary objectForKey:@"taskGuid"] syncStatus:@"0" deleted:@"2"];
                
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ProjectsFinshedSyncing" object:nil];
            [NTHelpers setSyncStatus:@"1"];
        }
                    failure :^ (NSURLSessionDataTask * sessionTask, NSError * error) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ProjectsFinshedSyncing" object:nil];
                        NSLog(@"%@",[error localizedDescription]);
                    }];
    }
    
    
}

#pragma mark - User

- (void)createUser:(NSString*)userName withPassword:(NSString*)password{
    AFHTTPSessionManager * generalManager = self.generalManager;
    
    generalManager.requestSerializer = [AFJSONRequestSerializer serializer];
    generalManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    
    
    NSDictionary * dict = @{@"userName" : userName,
                            @"password" : password
                            };
    
    [generalManager POST : [NSString stringWithFormat : @"user"] parameters: dict success :^ (NSURLSessionDataTask * task, id responseObject) {
        
        [self.delegate createResponse:(NSDictionary * )responseObject];
        
    }failure :^ (NSURLSessionDataTask * task, NSError * error) {
        [self.delegate loginFailed:error];
    }];
    
}

- (void) getUser:(NSString*)userName{
    AFHTTPSessionManager * generalManager = self.generalManager;
    generalManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [generalManager GET:[NSString stringWithFormat : @"user/%@",userName] parameters: [NSDictionary dictionary] success :^ (NSURLSessionDataTask * task,id responseObject) {
        if([responseObject count]==0)
        {
            [self.delegate loginResponse:nil];
        }
        else if([responseObject count]==1)
        {
            NSDictionary * dictionary = [responseObject objectAtIndex:0];
            [self.delegate loginResponse:dictionary];
        }
    } failure :^ (NSURLSessionDataTask * task, NSError * error) {
        [self.delegate loginFailed:error];
    }];
}





@end
