//
//  main.m
//  iNappkinTester
//
//  Created by Luc Gaasch on 01/05/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NTAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
		return UIApplicationMain( argc, argv, nil, NSStringFromClass([NTAppDelegate class]) );
	}
}

