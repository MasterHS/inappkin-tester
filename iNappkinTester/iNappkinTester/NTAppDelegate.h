//
//  NTAppDelegate.h
//  iNappkinTester
//
//  Created by Luc Gaasch on 01/05/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

@import UIKit;

@interface NTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;
// @property (nonatomic) NSMutableArray *tasks;

@end
