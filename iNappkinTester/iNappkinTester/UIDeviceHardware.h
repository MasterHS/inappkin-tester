//
//  UIDeviceHardware.m
//  iNappkinTester
//
//  Created by Safetli, Hazem on 19/12/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIDeviceHardware : NSObject

- (NSString *) platform;
- (NSString *) platformString;

@end