//
//  SlimServerRequestHandler.h
//  iNappkinDesigner
//
//  Created by Julian-Lennart Weigand on 19.08.14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <Foundation/Foundation.h>
@import Foundation;
@class Project,Sketch,TransitionButton,Task,TapEvent;

@protocol ServerRequestsDelegate
@required
-(void)loginResponse:(NSDictionary *) responseObject;
-(void)createResponse:(NSDictionary *) responseObject;
-(void)loginFailed:(NSError *)error;
@end


@interface SlimServerRequestHandler : NSObject<NSURLConnectionDataDelegate>
{
    NSMutableData *responseData;
}
@property (strong, nonatomic) AFHTTPSessionManager * generalManager;
@property (strong, nonatomic) AFHTTPSessionManager * imageDownloadManager;
@property (strong, nonatomic) NSString * baseURLStringForGeneralManager;

- (id)initWithBaseURLStringForGeneralFunctionality: (NSString *)baseURLStringForGeneralFunctionality;

@property (weak,nonatomic) id<ServerRequestsDelegate> delegate;

- (void) testServerURL;
- (void)startSyncingProcess;

- (void)createUser:(NSString*)userName withPassword:(NSString*)password;
- (void) getUser:(NSString*)userName;

@end

