//
//  NTTaskTutorialPageController.m
//  iNappkinTester
//
//  Created by Dan on 2/7/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NTTaskTutorialPageController.h"
#import "NTTaskTutorialController.h"

@interface NTTaskTutorialPageController ()

@end

@implementation NTTaskTutorialPageController

#pragma mark - private methods
- (NTTaskTutorialController *) viewControllerAtIndex : (NSUInteger) index
{
	if ( ([self.pageImages count] == 0) || (index >= [self.pageImages count]) ) {
		return nil;
	}

	NTTaskTutorialController * taskTutorialController =
		[self.storyboard instantiateViewControllerWithIdentifier : @"NTTaskTutorialController"];
	taskTutorialController.imageName = self.pageImages[index];
	taskTutorialController.labelText = self.pageTexts[index];
	taskTutorialController.pageIndex = index;
	taskTutorialController.pageCount = [self.pageImages count];
	taskTutorialController.task = self.task;

	return taskTutorialController;
}

#pragma mark - view controller methods

- (id) initWithNibName : (NSString *) nibNameOrNil bundle : (NSBundle *) nibBundleOrNil
{
	self = [super initWithNibName : nibNameOrNil bundle : nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void) viewDidLoad
{
	[super viewDidLoad];
	_pageImages = @[@"done_quit",@"shake"];
	_pageTexts  =
		@[
			@"Note: click \"Done\" to finish a task and upload data;\n click \"Quit\" to quit a task without saving data",
			@"Note: shake to show the navigation bar"];

	// create page view controller
	self.pageViewController =
		[self.storyboard instantiateViewControllerWithIdentifier : @"pageViewController"];
	self.pageViewController.dataSource = self;

	NTTaskTutorialController * startViewController = [self viewControllerAtIndex : 0];
	NSArray * viewControllers = @[startViewController];
	[self.pageViewController setViewControllers : viewControllers direction :
	 UIPageViewControllerNavigationDirectionForward animated : NO completion : nil];

	// self.pageViewController.view.frame = CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height-30);
	[self addChildViewController : _pageViewController];
	[self.view addSubview : _pageViewController.view];
	[self.pageViewController didMoveToParentViewController : self];

}

- (void) didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - Page View Controller Data Source
- (UIViewController *) pageViewController : (UIPageViewController *) pageViewController
	   viewControllerBeforeViewController : (UIViewController *) viewController
{
	NSUInteger index = ( (NTTaskTutorialController *) viewController ).pageIndex;

	if ( (index == 0) || (index == NSNotFound) ) {
		return nil;
	}
	index--;

	return [self viewControllerAtIndex : index];
}

- (UIViewController *) pageViewController : (UIPageViewController *) pageViewController
		viewControllerAfterViewController : (UIViewController *) viewController
{

	NSUInteger index = ( (NTTaskTutorialController *) viewController ).pageIndex;

	if (index == NSNotFound) {
		return nil;
	}
	index++;
	if (index == [self.pageImages count]) {
		return nil;
	}
	return [self viewControllerAtIndex : index];
}

- (NSInteger) presentationCountForPageViewController : (UIPageViewController *) pageViewController
{
	return [self.pageImages count];
}

- (NSInteger) presentationIndexForPageViewController : (UIPageViewController *) pageViewController
{
	return 0;
}

/*
   #pragma mark - Navigation

   // In a storyboard-based application, you will often want to do a little preparation before navigation
   - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
   {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
   }
 */

@end
