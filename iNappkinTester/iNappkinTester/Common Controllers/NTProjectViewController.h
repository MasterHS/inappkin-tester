//
//  NTProjectViewController.h
//  iNappkinTester
//
//  Created by Julian-Lennart Weigand on 26.08.14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NDTransitionButtonOverlayViewController.h"
#import "Project.h"
#import "Sketch.h"
#import "TransitionButton.h"

@interface NTProjectViewController : UIViewController


@property IBOutlet UIImageView * sketchDetailedImage;
@property IBOutlet NDTransitionButtonOverlayViewController *transitionButtonOverlay;
@property IBOutlet UIView *tutorialView;

@property NSIndexPath * selectedSketchIndexPath;
@property TransitionButton *currentlySelectedTransitionButton;
@property Sketch *currentlySelectedSketch;
@property Project *currentProject;

-(IBAction)transitionButtonPressed:(UIButton*)sender;

@end
