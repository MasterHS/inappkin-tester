//
//  NTProjectController.h
//  iNappkinTester
//
//  Created by Orest on 02/07/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Zeigt Detailinfos zu einem Projekt an (Name, Descrtiption)
 
 ### Der Switch um den Shake to Exit hinweis am Anfang anzeigen zu lassen gehoert hier nicht wirklich hin. Sowas sollte lieber in einen Settings Screen.
 */

@import UIKit;
#import "Project.h"

@interface NTProjectDetailViewController : UIViewController

@property (nonatomic, strong) Project * currentProject;
@property (weak, nonatomic) IBOutlet UITextField * projectNameField;
@property (weak, nonatomic) IBOutlet UITextView * projectDescriptionView;
@property (weak, nonatomic) IBOutlet UISwitch * showShakeSwitch;
@property (nonatomic, strong) NSUserDefaults * defaults;
@property (weak, nonatomic) IBOutlet UILabel *projectOwnerLabel;

@end
