//
//  NTTasksController.m
//  iNappkinTester
//
//  Created by Dan on 18/6/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NTTasksViewController.h"
#import "NTProjectDetailViewController.h"
#import "NTTaskViewController.h"
#import "NTTaskTutorialViewController.h"
#import "NTTaskTutorialPageController.h"
#import "SlimServerRequestHandler.h"
#import "NTHelpers.h"
#import "CoreDataManager.h"
@interface NTTasksViewController ()

@end

@implementation NTTasksViewController
//NSArray* sketches;
//NSArray* tasks;


- (id) initWithNibName : (NSString *) nibNameOrNil bundle : (NSBundle *) nibBundleOrNil{
	self = [super initWithNibName : nibNameOrNil bundle : nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void) viewDidLoad{
	[super viewDidLoad];
    //[self loadData];
    self.navigationItem.title = self.currentProject.projectName;
}


#pragma mark - TOOLS
- (NSUserDefaults *) defaults{
	if (!_defaults) {
		_defaults = [NSUserDefaults standardUserDefaults];
	}
	return _defaults;
}

- (BOOL) showAlertViewWithFailedCases : (NSUInteger) failedCases totalCases : (NSUInteger) total index : (NSUInteger) index{
	if ( index != total) return NO;
    
	if (failedCases != 0) {
		NSString * title =
        [NSString stringWithFormat : @"%lu percent tapEvents failed to upload.",
         (unsigned long) failedCases * 100 / total];
		NSLog(@"%lu, %lu", (unsigned long) failedCases, (unsigned long) total);
		UIAlertView * message =
        [[UIAlertView alloc]initWithTitle : nil message : title delegate : nil cancelButtonTitle : @"OK"
                        otherButtonTitles : nil];
		[message show];
		return YES;
	}else{
		return NO;
	}
}

#pragma mark - ACTIONS
- (IBAction) taskComplete : (UIStoryboardSegue *) segue{
	if ([segue.sourceViewController isKindOfClass : [NTTaskViewController class]]) {
		NTTaskViewController * taskController = (NTTaskViewController *) segue.sourceViewController;
#ifdef DEBUG
		NSLog(@"task name: %@",taskController.task.taskName);
        self.currentTask=taskController.task;
#endif

        [self pushTaskInfoToServer];
		//[taskController.task removeAllTapEvents];
	}
}

-(void)pushTaskInfoToServer
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Start syncing with the server?" message:@"Do you want to send test data to the server?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {

        self.navigationController.navigationBar.userInteractionEnabled = NO;
        //self.updateIndicatorView.hidden= NO;
        //[self.view bringSubviewToFront:self.updateIndicatorView];
        
        SlimServerRequestHandler *requestHandler= [[SlimServerRequestHandler alloc] initWithBaseURLStringForGeneralFunctionality:[NTHelpers getBaseServerName]];
        
        [requestHandler startSyncingProcess];
        //[self.projectsCollectionView reloadData];
        
    }
    else //delete all taps and stats
    {
//        NSArray* taps=[[CoreDataManager sharedManager]getTapEventsByTaskId:self.currentTask.guid];
//        NSArray* stats=[[CoreDataManager sharedManager]getTaskStats:self.currentTask.guid];
//        for (TapEvent *tap in taps) {
//            
//        }
//        for (TaskStat *stat in stats) {
//            
//        }
    }
}
- (IBAction) taskQuit : (UIStoryboardSegue *) segue{
	//NTTaskViewController * taskController = (NTTaskViewController *) segue.sourceViewController;
	//[taskController.task removeAllTapEvents];
}

#pragma mark - Protocols
- (void) masterController : (NTTasksMasterController *) masterController didSelectTask : (Task *)task{
	self.detailController.task = task;
	[self.detailController reloadData];
}

#pragma mark - Navigation
- (BOOL) shouldPerformSegueWithIdentifier : (NSString *) identifier sender : (id) sender{
	if ([identifier isEqualToString : @"start a task"]) {
		return ![self.defaults boolForKey : @"showShake"];
	}
	if ([identifier isEqualToString : @"task tutorial"]) {
		return [self.defaults boolForKey : @"showShake"];
	}
	return YES;
}

- (void) prepareForSegue : (UIStoryboardSegue *) segue sender : (id) sender{
	// if this is an embed segue then keep a reference to the view controller
	// that is being embeded
	if ([segue.identifier isEqualToString : @"embed tasks master view"]) {
		NTTasksMasterController * masterController =
        (NTTasksMasterController *) segue.destinationViewController;
		self.masterController = masterController;
		[self.masterController setDelegate : self];
        self.masterController.currentTask=self.detailController.task;
		self.masterController.currentProject = self.currentProject;
	}else if ([segue.identifier isEqualToString : @"embed task detail view"]) {
		NTTasksDetailViewController * detailController =
        (NTTasksDetailViewController *) segue.destinationViewController;
		self.detailController = detailController;
        self.detailController.currentProject=self.currentProject;
        
		[self.detailController setDelegate : self];
	}else if ([segue.identifier isEqualToString : @"project detail"]) {
		if ([segue.destinationViewController isKindOfClass : [UIViewController class]]) {
			NTProjectDetailViewController * projectController = segue.destinationViewController;
			projectController.currentProject = self.currentProject;
		}
	}else if ([segue.identifier isEqualToString : @"start a task"]) {
		if ([segue.destinationViewController isKindOfClass : [UINavigationController class]]) {
			UINavigationController * nagivationController = segue.destinationViewController;
			NTTaskViewController * dst = nagivationController.viewControllers[0];
            dst.currentProejct=self.currentProject;
			dst.task = self.detailController.task;
		}
	}else if ([segue.identifier isEqualToString : @"task tutorial"]) {
		if ([segue.destinationViewController isKindOfClass : [UIViewController class]]) {
			NTTaskTutorialPageController * tutorialController = segue.destinationViewController;
            tutorialController.currentProject=self.currentProject;
            tutorialController.task = self.detailController.task;

		}
	}
}

@end
