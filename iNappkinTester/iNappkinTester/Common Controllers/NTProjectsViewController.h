//
//  NTMenuViewController.h
//  iNappkinTester
//
//  Created by Fei Pan on 21/05/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Ruft Projekte vom Server ab und zeigt sie in einer CollectionView an.
 
 */

@import UIKit;
@interface NTProjectsViewController : UIViewController < UICollectionViewDataSource, UICollectionViewDelegate>{
    NSArray *allProjects;    

}

@property (weak, nonatomic) IBOutlet UICollectionView * projectsCollectionView;

@property IBOutlet UIView *updateIndicatorView;

@end
