//
//  NTMainMenuCollectionViewCell.h
//  iNappkinTester
//
//  Created by Orest on 02/07/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Wird von NTProjectsController verwendet um Projekte in der UICollection zu repraesentieren.
 */

@import UIKit;

@interface NTProjectCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView * projectThumbnail;
@property (weak, nonatomic) IBOutlet UILabel * projectNameLabel;

@end
