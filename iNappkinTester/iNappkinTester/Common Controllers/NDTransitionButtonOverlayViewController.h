//
//  NDTransitionButtonOverlayViewController.h
//  iNappkinDesigner
//
//  Created by Julian-Lennart Weigand on 18.08.14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sketch.h"
#import "TransitionButton.h"

@interface NDTransitionButtonOverlayViewController : UIView <UIGestureRecognizerDelegate>

@property Sketch *currentSketch;

@property (nonatomic,strong) id delegate;

-(void)setNewActiveSketch:(Sketch*)sketch;
- (void) drawAllButtons;

@end
