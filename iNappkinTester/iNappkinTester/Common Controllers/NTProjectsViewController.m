//
//  NTMenuViewControlleriPad.m
//  iNappkinTester
//
//  Created by Fei Pan on 21/05/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NTProjectsViewController.h"
#import "NTTasksViewController.h"
#import "NTProjectCollectionViewCell.h"
#import "CoreDataManager.h"
#import "SlimServerRequestHandler.h"
#import "NTHelpers.h"
#import "Project.h"
#import "Sketch.h"
#import "NTProjectViewController.h"
#import "NTTasksViewController.h"

@interface NTProjectsViewController ()

@end

@implementation NTProjectsViewController

#pragma mark - private methods

- (void) setShowShakeDefaults{
	NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
	if ([defaults objectForKey : @"showShake"] == nil) {
		[defaults setBool : YES forKey : @"showShake"];
		[defaults synchronize];
	}
}

#pragma mark - view controller methods
- (void) viewDidLoad{
	[super viewDidLoad];
	[self getAllProjects];
    [self loadProjectsFromServer:self];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeUpdateView) name:@"ProjectsFinshedSyncing" object:nil];
    
	[self setShowShakeDefaults];
}

- (void) viewDidAppear : (BOOL) animated{
	[super viewDidAppear : animated];
	[self.projectsCollectionView reloadData];
}
-(void)getAllProjects{
    allProjects=[[CoreDataManager sharedManager]getAvailableProjects];
    [self.projectsCollectionView reloadData];
}

#pragma mark - UICollectionView Data Source
- (NSInteger) numberOfSectionsInCollectionView : (UICollectionView *) collectionView{
	return 1;
}

- (NSInteger) collectionView : (UICollectionView *) collectionView numberOfItemsInSection : (NSInteger)section{
	return [allProjects count];
}

- (UICollectionViewCell *) collectionView : (UICollectionView *) collectionView cellForItemAtIndexPath: (NSIndexPath *) indexPath{
	NTProjectCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"NTMainMenuCollectionViewCell" forIndexPath : indexPath];
    
    Project *project= [allProjects objectAtIndex:indexPath.row];
    
    //Cell setup
	cell.projectNameLabel.text = project.projectName;
	cell.projectNameLabel.numberOfLines = 0; // allow line breaks
	cell.projectNameLabel.textAlignment = NSTextAlignmentCenter; // center align
    
	// Project thumbnail
	UIImage * image;
        NSArray* sketches=[[CoreDataManager sharedManager]getSketches:project.guid];
    if ([sketches count]> 0) {
        Sketch *tSketch = [sketches objectAtIndex:0];
        image= [UIImage imageWithData: tSketch.imageFile];
    }
    else image = [UIImage imageNamed : @"ProjectDummyThumbnail"];
    cell.projectThumbnail.image = image;
    
	return cell;
}

#pragma mark - private methods
- (IBAction)loadProjectsFromServer:(id)sender {
    //[[CoreDataManager sharedManager] resetDatastore];
    self.navigationController.navigationBar.userInteractionEnabled = NO;
    self.updateIndicatorView.hidden= NO;
    [self.view bringSubviewToFront:self.updateIndicatorView];
    
    SlimServerRequestHandler *requestHandler= [[SlimServerRequestHandler alloc] initWithBaseURLStringForGeneralFunctionality:[NTHelpers getBaseServerName]];
    
    //[requestHandler downloadAllProjectsWithAllSubdata];
    [requestHandler startSyncingProcess];
    [self.projectsCollectionView reloadData];
}

- (IBAction)changeConnectionSettings:(id)sender {
    
    
}


#pragma mark - segue
- (void) prepareForSegue : (UIStoryboardSegue *) segue sender : (id) sender{
	if ([sender isKindOfClass : [UICollectionViewCell class]]) {
		NSIndexPath * indexPath = [self.projectsCollectionView indexPathForCell : sender];
		if (indexPath) {
			if ([segue.identifier isEqualToString : @"OpenProject"]) {
                //NTProjectViewController *aVC = segue.destinationViewController;
                NTTasksViewController *tasksController=segue.destinationViewController;
                Project * selectedProject = [allProjects objectAtIndex:indexPath.row];
                tasksController.currentProject = selectedProject;
			}
		}
	}
}

#pragma mark - Notification Methoden
-(void)removeUpdateView{
    [self getAllProjects];
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    self.updateIndicatorView.hidden = YES;
}

- (void) viewWillAppear : (BOOL) animated{
    [self getAllProjects];
    [self.projectsCollectionView reloadData];
}




@end
