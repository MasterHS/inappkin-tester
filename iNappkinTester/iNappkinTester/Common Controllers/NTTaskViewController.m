//
//  NTTaskController.m
//  iNappkinTester
//
//  Created by Dan on 22/6/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NTTaskViewController.h"
#import "NTHelpers.h"
#import "TransitionButton.h"
#import "CoreDataManager.h"
#import "SlimServerRequestHandler.h"
#import "UIDeviceHardware.h"
#import "CoreDataManager.h"
@interface NTTaskViewController ()
@property (nonatomic) NSDate* taskStartTime;
@property (nonatomic) NSDate* taskEndTime;
@property (nonatomic) NSDate* sketchEnteredTime;//what time the sketch was entered
@end

@implementation NTTaskViewController

#pragma mark- private methods
- (UIImageView *) imageView
{
    if (!_imageView) {
        _imageView = [[UIImageView alloc]init];

    }
    return _imageView;
}

#pragma  mark - view controller methods

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.title = self.task.taskName;
    Sketch* firstSketch=[[CoreDataManager sharedManager]getDataById:self.task.firstSketchGuid inTable:@"Sketch"];
    self.imageView.image = [UIImage imageWithData:firstSketch.imageFile];
    self.sketch = firstSketch;
    self.taskStartTime=[NSDate date];
    self.sketchEnteredTime=[NSDate date];
        [self.navigationController setNavigationBarHidden : YES animated : YES];
}

- (IBAction) doneButtonClicked : (id) sender
{
    
}

- (IBAction) tap : (UITapGestureRecognizer *) sender
{
    
    
    CGPoint tapPoint = [sender locationInView : self.imageView];
    NSString* timeSpentonSketch=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSinceDate:self.sketchEnteredTime]];
    self.sketchEnteredTime=[NSDate date];
    
    //NSDictionary* aspects=[NTHelpers getAspectRatioAndScreensizeForDeviceType:self.currentProejct.deviceType];
    //TapEvent* tap=[[CoreDataManager sharedManager]createTapEvent:self.sketch withLocation:tapPoint timeSpentOnSketch:timeSpentonSketch];
    NSString* deviceType=[[[UIDeviceHardware alloc]init ]platformString];
    NSDictionary* tapData=@{@"deviceType":deviceType,@"timeOnSketch":timeSpentonSketch,@"xAxis":[NSNumber numberWithFloat:tapPoint.x],@"yAxis":[NSNumber numberWithFloat:tapPoint.y]};
    
    [[CoreDataManager sharedManager]createOrModifyTapEvent:tapData tapGuid:nil sketchGuid:self.sketch.guid taskGuid:self.task.guid syncStatus:@"1" deleted:@"0"];
    NSArray* transitionButtons=[[CoreDataManager sharedManager]getTransitionButtons:self.sketch.guid];
    for (TransitionButton* button in transitionButtons) {
        float absolutX= ([button.xAxis floatValue]/100)*self.imageView.frame.size.width;
        float absolutY= ([button.yAxis floatValue]/100)*self.imageView.frame.size.height;
        float absolutWidth = ([button.width floatValue]/100)*self.imageView.frame.size.width;
        float absolutHeight = ([button.height floatValue]/100)*self.imageView.frame.size.height;
        CGRect absolutRect= CGRectMake(absolutX, absolutY, absolutWidth, absolutHeight);
        

        if ( CGRectContainsPoint(absolutRect, tapPoint) ) {
            if (![button.destinationSketchGuid isEqualToString:@""]) {
                self.sketch = [[CoreDataManager sharedManager]getDataById:button.destinationSketchGuid inTable:@"Sketch"];
                NSLog(@"sketch order %@",self.sketch.sketchOrderNumber);
                // display sketch with animation
                self.imageView.image = [UIImage imageWithData:self.sketch.imageFile];
                CATransition * transition = [CATransition animation];
                transition.duration = 0.3f;
                transition.timingFunction =
                [CAMediaTimingFunction functionWithName : kCAMediaTimingFunctionLinear];
                transition.type = kCATransitionFromLeft;
                
                [self.imageView.layer addAnimation : transition forKey : nil];
                return;
            }
        }
    }
    
}

- (BOOL) canBecomeFirstResponder
{
    return YES;
}

- (void) motionEnded : (UIEventSubtype) motion withEvent : (UIEvent *) event
{
    if (motion == UIEventSubtypeMotionShake) {
        NSString* timeSpentonSketch=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSinceDate:self.sketchEnteredTime]];
        self.sketchEnteredTime=[NSDate date];
            NSString* deviceType=[[[UIDeviceHardware alloc]init ]platformString];
        NSDictionary* tapData=@{@"deviceType":deviceType,@"timeOnSketch":timeSpentonSketch,@"xAxis":[NSNumber numberWithInteger:-20],@"yAxis":[NSNumber numberWithInteger:-20]};
        
        [[CoreDataManager sharedManager]createOrModifyTapEvent:tapData tapGuid:nil sketchGuid:self.sketch.guid taskGuid:self.task.guid syncStatus:@"1" deleted:@"0"];
        
        [self.navigationController setNavigationBarHidden : NO animated : YES];
        self.taskEndTime=[NSDate date];
        self.taskDuration=[NSString stringWithFormat:@"%f",[self.taskEndTime timeIntervalSinceDate:self.taskStartTime]];
        NSString* taskSuccessfulFlag=@"NO";
        if([self.sketch.guid isEqualToString: self.task.destinationSketchGuid])//task is successfull
        {
            taskSuccessfulFlag=@"YES";
        }
        [[CoreDataManager sharedManager]createOrModifyTaskStat:@{@"deviceIdentifier":[NTHelpers getDevId], @"taskCompletionTime":self.taskDuration,@"taskCompleted":taskSuccessfulFlag} statGuid:nil taskGuid:self.task.guid syncStatus:@"1" deleted:@"0"];
        
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
