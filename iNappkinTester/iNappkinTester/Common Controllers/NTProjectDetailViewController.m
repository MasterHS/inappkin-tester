//
//  NTProjectController.m
//  iNappkinTester
//
//  Created by Orest on 02/07/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NTProjectDetailViewController.h"
//#import "ADSProject.h"
#import "NTHelpers.h"

@interface NTProjectDetailViewController ()

@end

@implementation NTProjectDetailViewController

- (NSUserDefaults *) defaults{
	if (!_defaults) {
		_defaults = [NSUserDefaults standardUserDefaults];
	}
	return _defaults;
}

- (void) setUpShowShakeSwitch{
	self.showShakeSwitch.on = [self.defaults boolForKey : @"showShake"];
}

- (void) setProjectDetails{
	self.projectNameField.text = self.currentProject.projectName;
	self.projectDescriptionView.text = self.currentProject.projectDescription;
    self.projectOwnerLabel.text=self.currentProject.userName;
}

- (void) viewDidLoad{
	[super viewDidLoad];
	[self setProjectDetails];
	[self setUpShowShakeSwitch];
}

- (void) viewWillDisappear : (BOOL) animated{
	[self.defaults setBool : self.showShakeSwitch.on forKey : @"showShake"];
	[self.defaults synchronize];
}

- (IBAction) showShakeSwitchClicked : (id) sender{
	[self.defaults setBool : self.showShakeSwitch.on forKey : @"showShake"];
}

@end
