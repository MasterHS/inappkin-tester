//
//  NTTasksController.h
//  iNappkinTester
//
//  Created by Dan on 18/6/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Die View beinhaltet in Containern die Views von NTTaskMasterController und NTTasksDetailcontroller.
 NTTasksMastercontroller zeigt links in der TV alle Tasks des Projekts an.
 NTTasksDetailController zeigt rechts alle Infos zu einem ausgewählten Task und die Moeglichkeit diesen per Knopfdruk zu beginnen an.
 
 ### Den TaskTutorialController wuerde ich nicht mit in die Navigation packen sondern lieber beim Aufruf des NTTaskController eine Abfrage auf den Default (Tutorial anzeigen/nicht anzeigen) machen und dann evtl. ein ModalVIew oder Overlay verwenden.
 */

@import UIKit;
#import "NTTasksMasterController.h"
#import "NTTasksDetailViewController.h"
#import "Project.h"
@class ADSServerRequestHandler;

@interface NTTasksViewController : UIViewController <NTTasksMasterControllerDelegate, NTTasksDetailControllerDelegate>

@property (nonatomic, strong) Project* currentProject;
@property (nonatomic, strong) Task* currentTask;
@property (nonatomic, strong) ADSServerRequestHandler * serverRequestHandler;
@property (nonatomic, strong) NTTasksMasterController * masterController;
@property (nonatomic, strong) NTTasksDetailViewController * detailController;
@property (nonatomic, strong) NSUserDefaults * defaults;

@end
