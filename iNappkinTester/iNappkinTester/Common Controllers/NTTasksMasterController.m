//
//  NTTasksMasterController.m
//  iNappkinTester
//
//  Created by Dan on 18/6/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NTTasksMasterController.h"
//#import "ADSTasksBoard.h"
#import "Task.h"
#import "CoreDataManager.h"
@interface NTTasksMasterController ()

@end

@implementation NTTasksMasterController
NSArray* tasks;

-(void)loadData
{
    tasks=[[CoreDataManager sharedManager]getTasks:self.currentProject.guid];
    //sketches=[[CoreDataManager sharedManager]getSketches:self.currentProject.guid];
}

#pragma mark - view controller methods
- (void) viewDidLoad{
    [self loadData];
	[super viewDidLoad];
}

- (void) viewDidAppear : (BOOL) animated{
	[super viewDidAppear : animated];
	[self.tableView reloadData];
}


#pragma mark - Table view data source
- (NSInteger) numberOfSectionsInTableView : (UITableView *) tableView{
	return 1;
}

- (NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection : (NSInteger) section{
	return [tasks count];
}

- (UITableViewCell *) tableView : (UITableView *) tableView cellForRowAtIndexPath : (NSIndexPath *)indexPath{
	UITableViewCell * cell;
	cell = [tableView dequeueReusableCellWithIdentifier : @"task cell" forIndexPath : indexPath];
	Task * task = [tasks objectAtIndex: indexPath.row];
	cell.textLabel.text = task.taskName;
	return cell;
}

- (void) tableView : (UITableView *) tableView didSelectRowAtIndexPath : (NSIndexPath *) indexPath{
	[self.delegate masterController : self didSelectTask : [tasks objectAtIndex : indexPath.row]];
}


@end
