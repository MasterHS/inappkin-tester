//
//  NTTaskTutorialPageController.m
//  iNappkinTester
//
//  Created by Dan on 2/7/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NTTaskTutorialPageController.h"
#import "NTTaskTutorialViewController.h"

@interface NTTaskTutorialPageController ()

@end

@implementation NTTaskTutorialPageController

#pragma mark - private methods
- (NTTaskTutorialViewController *) viewControllerAtIndex : (NSUInteger) index{
	if ( ([self.pageImages count] == 0) || (index >= [self.pageImages count]) ) {
		return nil;
	}
    
	NTTaskTutorialViewController * taskTutorialController =
    [self.storyboard instantiateViewControllerWithIdentifier : @"NTTaskTutorialController"];
	taskTutorialController.imageName = self.pageImages[index];
	taskTutorialController.labelText = self.pageTexts[index];
	taskTutorialController.pageIndex = index;
	taskTutorialController.pageCount = [self.pageImages count];
	taskTutorialController.task = self.task;
    taskTutorialController.currentProject=self.currentProject;
	return taskTutorialController;
}

#pragma mark - view controller methods

- (id) initWithNibName : (NSString *) nibNameOrNil bundle : (NSBundle *) nibBundleOrNil{
	self = [super initWithNibName : nibNameOrNil bundle : nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void) viewDidLoad{
	[super viewDidLoad];
	self.pageImages = @[@"Shake", @"ButtonTutorial"];
    
	// create page view controller
	self.pageViewController =
    [self.storyboard instantiateViewControllerWithIdentifier : @"pageViewController"];
	self.pageViewController.dataSource = self;
    
	NTTaskTutorialViewController * startViewController = [self viewControllerAtIndex : 0];
	NSArray * viewControllers = @[startViewController];
	[self.pageViewController setViewControllers: viewControllers direction: UIPageViewControllerNavigationDirectionForward animated: NO completion: nil];
    
	// self.pageViewController.view.frame = CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height-30);
	[self addChildViewController : _pageViewController];
	[self.view addSubview : _pageViewController.view];
	[self.pageViewController didMoveToParentViewController : self];
    
}

#pragma mark - Page View Controller Data Source
- (UIViewController *) pageViewController : (UIPageViewController *) pageViewController viewControllerBeforeViewController : (UIViewController *) viewController{
	NSUInteger index = ( (NTTaskTutorialViewController *) viewController ).pageIndex;
    
	if ( (index == 0) || (index == NSNotFound) ) {
		return nil;
	}
	index--;
    
	return [self viewControllerAtIndex : index];
}

- (UIViewController *) pageViewController : (UIPageViewController *) pageViewController viewControllerAfterViewController: (UIViewController *) viewController{
	NSUInteger index = ( (NTTaskTutorialViewController *) viewController ).pageIndex;
    
	if (index == NSNotFound) {
		return nil;
	}
	index++;
	if (index == [self.pageImages count]) {
		return nil;
	}
	return [self viewControllerAtIndex : index];
}

- (NSInteger) presentationCountForPageViewController: (UIPageViewController *) pageViewController{
	return [self.pageImages count];
}

- (NSInteger) presentationIndexForPageViewController: (UIPageViewController *) pageViewController{
	return 0;
}


@end
