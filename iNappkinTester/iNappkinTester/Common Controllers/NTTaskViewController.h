//
//  NTTaskController.h
//  iNappkinTester
//
//  Created by Dan on 22/6/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Die View in der der User den Task bearbeitet.
 Zeigt die Sketches an und macht transitions wenn der User in die vorgesehenen Areas tapt.
 Trackt die Taps.
 
 Alles schoen schlank implementiert. Gefaellt mir auf den ersten Blick sehr gut.
 */

@import UIKit;
#import "Task.h"
#import "Project.h"
#import "Sketch.h"


@interface NTTaskViewController : UIViewController

@property (nonatomic,strong) Project* currentProejct;
@property (nonatomic,strong) Task* task;
@property (strong, nonatomic) IBOutlet UIImageView * imageView;
@property (strong, nonatomic) Sketch * sketch;
@property (nonatomic, strong) NSString * taskDuration;
@end
