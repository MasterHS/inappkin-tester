//
//  NTSettingsViewController.m
//  iNappkinTester
//
//  Created by Safetli, Hazem on 31/10/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NTSettingsViewController.h"
#import "NTHelpers.h"
#import "SlimServerRequestHandler.h"
@interface NTSettingsViewController ()

@end

@implementation NTSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.serverTextField.text = [NTHelpers getBaseServerName];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)testAddressInput:(id)sender{
    SlimServerRequestHandler *serverRequesthandler = [[SlimServerRequestHandler alloc]initWithBaseURLStringForGeneralFunctionality:self.serverTextField.text];
    [serverRequesthandler testServerURL];
}
//-(IBAction)cancelAndBack:(id)sender{
//    [self dismissViewControllerAnimated:YES completion:nil];
//}

-(IBAction)saveAndBack:(id)sender{
    [[NSUserDefaults standardUserDefaults] setObject:self.serverTextField.text forKey:@"baseServerName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction)resetToFactoryDefaults:(id)sender{
    self.serverTextField.text = BaseURLString;
    [[NSUserDefaults standardUserDefaults] setObject:BaseURLString forKey:@"baseServerName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(IBAction)resetLocalDataStorage:(id)sender{
   // [[CoreDataManager sharedManager]resetDatastore];
}
- (IBAction)cancelAndBack:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


@end
