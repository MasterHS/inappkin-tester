//
//  NTProjectsSegmentController.h
//  iNappkinTester
//
//  Created by Md.Habibur Rahman on 26/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Die Rootview der App.
 Zeigt alle verfügbaren Projekte an.
 
 ### Der Switsch funktioniert nicht.
 ###BUGFIX Auf dem iPad(Simulator) werden keine Projekte synchronisiert.
 
 ### Diesen und den NTProjectsLocalVC loeschen und nur NTProjectsController verwenden? Warum sollte der Tester ueberhaupt lokale Projekte haben? Wird nicht gleich alles was der Tester macht auf den Server gepusht?
 */

@import UIKit;

@interface NTProjectsSegmentController : UIViewController

@end
