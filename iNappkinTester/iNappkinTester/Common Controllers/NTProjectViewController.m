//
//  NTProjectViewController.m
//  iNappkinTester
//
//  Created by Julian-Lennart Weigand on 26.08.14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NTProjectViewController.h"
#import "CoreDataManager.h"

@interface NTProjectViewController ()

@end

@implementation NTProjectViewController

//der Frame vom TransitionButtonOverlay muss hier so haeufig gesetzt werden weil sich der Frame der ImageView immer beim Ein-/Ausblenden der Navigationbar veraendert

- (void)viewDidLoad{
    [super viewDidLoad];
    self.transitionButtonOverlay.delegate = self;
    
    [self.navigationController setNavigationBarHidden:YES];
    [self setTitle:self.currentProject.projectName];
    NSArray* sketches=[[CoreDataManager sharedManager]getSketches:self.currentProject.guid];
    if ([sketches count]>0) {
        self.currentlySelectedSketch = [sketches objectAtIndex:0];
    }
    sketches=nil;
    UIImage *tImage=[UIImage imageWithData:self.currentlySelectedSketch.imageFile];
    [self.sketchDetailedImage setImage:tImage];
    self.transitionButtonOverlay.frame= [self getDisplayRectForCurrentImage];
    [self.transitionButtonOverlay setNewActiveSketch:self.currentlySelectedSketch];
}

-(IBAction)transitionButtonPressed:(UIButton*)sender{
    NSInteger selectedButtonIndex=sender.tag;
    NSArray* transitionButtons=[[CoreDataManager sharedManager]getTransitionButtons:self.currentlySelectedSketch.guid];
    self.currentlySelectedTransitionButton = [transitionButtons objectAtIndex:selectedButtonIndex];
    transitionButtons=nil;
    Sketch *toSketch=[[CoreDataManager sharedManager]getDataById:self.currentlySelectedTransitionButton.destinationSketchGuid inTable:@"Sketch"];
    NSArray* sketches=[[CoreDataManager sharedManager]getSketches:self.currentProject.guid];
    NSUInteger toSketchIndex=  [sketches indexOfObject:toSketch];
    sketches=nil;
    if (toSketchIndex != NSNotFound) {
        self.selectedSketchIndexPath = [NSIndexPath indexPathForItem: toSketchIndex inSection: 0];
        self.currentlySelectedSketch= toSketch;
        
        UIImage *tImage=[UIImage imageWithData:toSketch.imageFile];
        [self.sketchDetailedImage setImage:tImage];
        self.transitionButtonOverlay.frame= [self getDisplayRectForCurrentImage];
        [self.transitionButtonOverlay setNewActiveSketch:self.currentlySelectedSketch];
    }
}

-(IBAction)hideTutorial:(id)sender{
    self.tutorialView.hidden=YES;
    self.transitionButtonOverlay.frame= [self getDisplayRectForCurrentImage];
    [self.transitionButtonOverlay setNewActiveSketch:self.currentlySelectedSketch];
}


- (void) motionEnded : (UIEventSubtype) motion withEvent : (UIEvent *) event{
	if (motion == UIEventSubtypeMotionShake) {
		[self.navigationController setNavigationBarHidden: NO animated: YES];
        
        self.transitionButtonOverlay.frame= [self getDisplayRectForCurrentImage];
        [self.transitionButtonOverlay setNewActiveSketch:self.currentlySelectedSketch];
        
        NSTimer *tTimer= [NSTimer timerWithTimeInterval:5.0 target:self selector:@selector(hideNavBar) userInfo:nil repeats:NO];
        [[NSRunLoop mainRunLoop] addTimer:tTimer forMode:NSDefaultRunLoopMode];
	}
}

-(void)hideNavBar{
    [self.navigationController setNavigationBarHidden: YES animated: YES];
    self.transitionButtonOverlay.frame= [self getDisplayRectForCurrentImage];
    [self.transitionButtonOverlay setNewActiveSketch:self.currentlySelectedSketch];
}

-(CGRect)getDisplayRectForCurrentImage{
    UIImageView *iv = self.sketchDetailedImage;
    CGSize imageSize = iv.image.size;
    CGFloat imageScale = fminf(CGRectGetWidth(iv.bounds)/imageSize.width, CGRectGetHeight(iv.bounds)/imageSize.height);
    CGSize scaledImageSize = CGSizeMake(imageSize.width*imageScale, imageSize.height*imageScale);
    CGRect imageFrame = CGRectMake(roundf(0.5f*(CGRectGetWidth(iv.bounds)-scaledImageSize.width))+self.sketchDetailedImage.frame.origin.x, roundf(0.5f*(CGRectGetHeight(iv.bounds)-scaledImageSize.height))+self.sketchDetailedImage.frame.origin.y, roundf(scaledImageSize.width), roundf(scaledImageSize.height));
    return imageFrame;
}


@end
