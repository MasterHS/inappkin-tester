//
//  NTTaskTutorialController.h
//  iNappkinTester
//
//  Created by Dan on 27/6/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Zeigt die Infos zum "Shake to exit" an.
 
 ### Ist derzeit in die Navigation eingebaut. Sollte lieber per Modal/Overlay in den NTTaskController eingefügt werden
 */

@import UIKit;
#import "Task.h"
#import "Project.h"
@interface NTTaskTutorialViewController : UIViewController
@property (nonatomic, strong) Task * task;
@property (nonatomic, strong) Project * currentProject;
@property (nonatomic, assign) NSUInteger pageIndex;
@property (nonatomic, assign) NSUInteger pageCount;
@property (nonatomic, strong) NSString * imageName;
@property (nonatomic, strong) NSString * labelText;

@property (nonatomic, strong) NSUserDefaults * defaults;
@property (weak, nonatomic) IBOutlet UIImageView * imageView;
@property (weak, nonatomic) IBOutlet UIImageView * lastImageView;
@property (weak, nonatomic) IBOutlet UILabel * shakeText;
@property (weak, nonatomic) IBOutlet UILabel * finishButtonText;
@property (weak, nonatomic) IBOutlet UILabel * cancelButtonText;
@property (weak, nonatomic) IBOutlet UIButton * skipButton;
@property (weak, nonatomic) IBOutlet UISwitch * showShakeSwitch;
@property (weak, nonatomic) IBOutlet UILabel * shouldShowTutorialText;

@end
