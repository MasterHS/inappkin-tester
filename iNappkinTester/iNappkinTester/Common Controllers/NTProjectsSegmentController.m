//
//  NTProjectsSegmentController.m
//  iNappkinTester
//
//  Created by Md.Habibur Rahman on 26/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NTProjectsSegmentController.h"
#import "NTProjectsLocalViewController.h"
#import "NTProjectsController.h"

@interface NTProjectsSegmentController ()

// Array of view controllers to switch between
@property (nonatomic, strong) NSArray * allViewControllers;

// Currently selected view controller
@property (nonatomic, strong) UIViewController * currentViewController;
@property (strong, nonatomic) IBOutlet UISegmentedControl * switchViewControllers;

- (IBAction) indexDidChangeForSegmentedControl : (id) sender;

@end

@implementation NTProjectsSegmentController


- (id) initWithNibName : (NSString *) nibNameOrNil bundle : (NSBundle *) nibBundleOrNil
{
	self = [super initWithNibName : nibNameOrNil bundle : nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void) viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.

	// Create the NTProjectsController controller
	NTProjectsController * viewcontrollerOnline =
		[self.storyboard instantiateViewControllerWithIdentifier : @"NTProjectsController"];

	// Create the NTProjectsLocalViewController view controller
	NTProjectsLocalViewController * viewcontrollerLocal =
		[self.storyboard instantiateViewControllerWithIdentifier : @"NTProjectsLocalViewController"];

	// Add NTProjectsController and NTProjectsLocalViewController view controllers to the array
	self.allViewControllers =
		[[NSArray alloc] initWithObjects : viewcontrollerOnline, viewcontrollerLocal, nil];


	// Ensure a view controller is loaded
	self.switchViewControllers.selectedSegmentIndex = 0;
	[self cycleFromViewController : self.currentViewController toViewController : [self.
																				   allViewControllers
																				   objectAtIndex : self.
																				   switchViewControllers
																				   .selectedSegmentIndex
	 ]];

}

#pragma mark - View controller switching and saving

- (void) cycleFromViewController : (UIViewController *) oldVC toViewController : (UIViewController *)
	newVC{

	// Do nothing if we are attempting to swap to the same view controller
	if (newVC == oldVC) return;

	// Check the newVC is non-nil otherwise expect a crash: NSInvalidArgumentException
	if (newVC) {

		// Set the new view controller frame (in this case to be the size of the available screen bounds)
		// Calulate any other frame animations here (e.g. for the oldVC)
		newVC.view.frame =
			CGRectMake( CGRectGetMinX(self.view.bounds), CGRectGetMinY(
							self.view.bounds), CGRectGetWidth(self.view.bounds),
						CGRectGetHeight(self.view.bounds) );

		// Check the oldVC is non-nil otherwise expect a crash: NSInvalidArgumentException
		if (oldVC) {

			// Start both the view controller transitions
			[oldVC willMoveToParentViewController : nil];
			[self addChildViewController : newVC];

			// Swap the view controllers
			// No frame animations in this code but these would go in the animations block
			[self transitionFromViewController : oldVC
							  toViewController : newVC
									  duration : 0.25
									   options : UIViewAnimationOptionLayoutSubviews
									animations :^ {}

									completion :^ (BOOL finished) {
			     // Finish both the view controller transitions
				 [oldVC removeFromParentViewController];
				 [newVC didMoveToParentViewController : self];
			     // Store a reference to the current controller
				 self.currentViewController = newVC;
			 }];

		} else {

			// Otherwise we are adding a view controller for the first time
			// Start the view controller transition
			[self addChildViewController : newVC];

			// Add the new view controller view to the ciew hierarchy
			[self.view addSubview : newVC.view];

			// End the view controller transition
			[newVC didMoveToParentViewController : self];

			// Store a reference to the current controller
			self.currentViewController = newVC;
		}
	}
}

#pragma mark - Segment Controller for Switch between Online and Local View Controller

- (IBAction) indexDidChangeForSegmentedControl : (UISegmentedControl *) sender
{
	NSUInteger index = sender.selectedSegmentIndex;
	if (UISegmentedControlNoSegment != index) {
		UIViewController * incomingViewController = [self.allViewControllers objectAtIndex : index];
		[self cycleFromViewController : self.currentViewController toViewController :
		 incomingViewController];
	}

}

- (IBAction) refreshButton : (id) sender
{
	if ([self.currentViewController isKindOfClass : [NTProjectsController class]]) {
		NTProjectsController * controller = (NTProjectsController *) self.currentViewController;
		[controller loadProjectsFromServer];
	}
}

@end
