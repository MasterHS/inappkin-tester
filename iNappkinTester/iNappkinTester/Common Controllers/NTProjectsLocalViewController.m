//
//  NTProjectsLocalViewController.m
//  iNappkinTester
//
//  Created by Md.Habibur Rahman on 26/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NTProjectsLocalViewController.h"

@interface NTProjectsLocalViewController ()

@end

@implementation NTProjectsLocalViewController

- (id) initWithNibName : (NSString *) nibNameOrNil bundle : (NSBundle *) nibBundleOrNil
{
	self = [super initWithNibName : nibNameOrNil bundle : nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void) viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void) didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


@end
