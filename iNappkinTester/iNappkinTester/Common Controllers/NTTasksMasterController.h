//
//  NTTasksMasterController.h
//  iNappkinTester
//
//  Created by Dan on 18/6/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Zeigt alle Tasks zu einem Projekt in TableView an.

 ### Die Weitergabe des Touch Events auf eine Cell in der TV ist unsauber. Lieber mit Notifications arbeiten.
 */

@import UIKit;
//@class ADSTasksBoard;
#import "Project.h"
#import "Task.h"
@class NTTasksMasterController;

@protocol NTTasksMasterControllerDelegate <NSObject>
- (void) masterController : (NTTasksMasterController *) masterController didSelectTask : (Task *)task;
@end

@interface NTTasksMasterController : UITableViewController
@property (strong, nonatomic) Project * currentProject;
@property (strong, nonatomic) Task * currentTask;
@property (nonatomic, retain) id<NTTasksMasterControllerDelegate> delegate;
@end