//
//  NTTaskTutorialController.m
//  iNappkinTester
//
//  Created by Dan on 27/6/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NTTaskTutorialViewController.h"
#import "NTTaskViewController.h"
#import "NTHelpers.h"
@interface NTTaskTutorialViewController ()

@end

@implementation NTTaskTutorialViewController

- (NSUserDefaults *) defaults{
	if (!_defaults) {
		_defaults = [NSUserDefaults standardUserDefaults];
	}
	return _defaults;
}

- (void) setUpShowShakeSwitch{
	self.showShakeSwitch.on = [self.defaults boolForKey : @"showShake"];
}

- (id) initWithNibName : (NSString *) nibNameOrNil bundle : (NSBundle *) nibBundleOrNil{
	self = [super initWithNibName : nibNameOrNil bundle : nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void) viewDidLoad{
	[super viewDidLoad];
	[self setUpShowShakeSwitch];
	self.imageView.image = [UIImage imageNamed : self.imageName];
	if (self.pageIndex != self.pageCount - 1 // not the last page
		&& UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
		self.skipButton.hidden = YES;
		self.showShakeSwitch.hidden = YES;
		self.shouldShowTutorialText.hidden = YES;
		self.shakeText.hidden = NO;
		self.finishButtonText.hidden = YES;
		self.cancelButtonText.hidden = YES;
		self.lastImageView.hidden = YES;
		self.imageView.hidden = NO;
	} else { // last page
		self.skipButton.hidden = NO;
		self.showShakeSwitch.hidden = NO;
		self.shouldShowTutorialText.hidden = NO;
		self.shakeText.hidden = YES;
		self.finishButtonText.hidden = NO;
		self.cancelButtonText.hidden = NO;
		self.lastImageView.hidden = NO;
		self.imageView.hidden = YES;
	}
	[self.shakeText sizeToFit];
}

- (IBAction) showShakeSwitchClicked : (id) sender{
	[self.defaults setBool : self.showShakeSwitch.on forKey : @"showShake"];
}

#pragma mark - Navigation
- (void) prepareForSegue : (UIStoryboardSegue *) segue sender : (id) sender{
    NSString* devType=[[UIDevice currentDevice] platformString] ;
	if ([segue.identifier isEqualToString : @"start task from tutorial"]) {
		if ([segue.destinationViewController isKindOfClass : [UINavigationController class]]) {
			UINavigationController * nagivationController = segue.destinationViewController;
			NTTaskViewController * dst = nagivationController.viewControllers[0];
            dst.currentProejct=self.currentProject;
			dst.task = self.task;

			[self.defaults setBool : self.showShakeSwitch.on forKey : @"showShake"];
			[self.defaults synchronize];
		}
	}
}

@end
