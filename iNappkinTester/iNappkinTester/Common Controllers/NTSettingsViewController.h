//
//  NTSettingsViewController.h
//  iNappkinTester
//
//  Created by Safetli, Hazem on 31/10/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NTSettingsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *serverTextField;

@end
