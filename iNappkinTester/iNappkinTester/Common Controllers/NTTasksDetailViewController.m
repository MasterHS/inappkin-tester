//
//  NTTasksDetailController.m
//  iNappkinTester
//
//  Created by Dan on 18/6/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NTTasksDetailViewController.h"
#import "CoreDataManager.h"

@interface NTTasksDetailViewController ()

@end

@implementation NTTasksDetailViewController
NSArray* sketches;
//NSArray* tasks;
#pragma mark - public methods
- (void) reloadData{
    //tasks=[[CoreDataManager sharedManager]getTasks:self.currentProject.guid];
    //sketches=[[CoreDataManager sharedManager]getSketches:self.currentProject.guid];
	if (self.task == nil){
		self.goButton.hidden = YES;
		self.taskName.text = nil;
		self.taskDescription.text = nil;
		self.taskNameTitle.text = nil;
		self.taskDescriptionTitle.text = nil;
	}
	else{
        Sketch* firstSketch=[[CoreDataManager sharedManager]getDataById:self.task.firstSketchGuid inTable:@"Sketch"];
		self.goButton.hidden = NO;
        self.firstSketch.image = [UIImage imageWithData:firstSketch.imageFile];
		self.taskName.text = [NSString stringWithFormat : @"%@",self.task.taskName];
		self.taskDescription.text = [NSString stringWithFormat : @"%@",self.task.taskDescription];
		self.taskNameTitle.text = @" Task";
		self.taskDescriptionTitle.text = @" Description";
		[self drawDescriptionBorder];
		[self drawTaskNameBorder];
        firstSketch=nil;
	}
}



#pragma mark - private methods
- (void) drawLeftBorder{
	UIView * leftView = [[UIView alloc]initWithFrame : CGRectMake(0, 0, 1, self.view.bounds.size.height)];
	leftView.opaque = YES;
	leftView.backgroundColor = [UIColor grayColor];
	[self.view addSubview : leftView];
}

- (void) drawTaskNameBorder{
	UIView * nameBorderView = [[UIView alloc]initWithFrame : CGRectMake(0, self.taskNameTitle.bounds.size.height - 1, self.taskNameTitle.bounds.size.width, 1)];
	nameBorderView.opaque = YES;
	nameBorderView.backgroundColor = [UIColor grayColor];
	[self.taskNameTitle addSubview : nameBorderView];
}

- (void) drawDescriptionBorder{
	UIView * desBorderView = [[UIView alloc]initWithFrame : CGRectMake(0, self.taskDescriptionTitle.bounds.size.height - 1, self.taskDescriptionTitle.bounds.size.width, 1)];
	desBorderView.opaque = YES;
	desBorderView.backgroundColor = [UIColor grayColor];
	[self.taskDescriptionTitle addSubview : desBorderView];
}

#pragma mark - view controller methods
- (void) viewDidLoad{
	[super viewDidLoad];
	[self drawLeftBorder];
	[self reloadData];
}

- (IBAction) goButtonPressed : (id) sender{
	if ([self.parentViewController shouldPerformSegueWithIdentifier : @"start a task" sender : self.
		 parentViewController]) {
		[self.parentViewController performSegueWithIdentifier : @"start a task" sender : self.
		 parentViewController];
	}else{
		[self.parentViewController performSegueWithIdentifier : @"task tutorial" sender : self.
		 parentViewController];
	}
}

@end
