//
//  TransitionButton.m
//  iNappkinDesigner
//
//  Created by Julian-Lennart Weigand on 20.08.14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "TransitionButton.h"
#import "Sketch.h"


@implementation TransitionButton

@dynamic height;
@dynamic width;
@dynamic xAxis;
@dynamic yAxis;
@dynamic destinationSketchGuid;
@dynamic last_modified;
@dynamic sync_status;
@dynamic is_deleted;
@dynamic guid;
@dynamic sketchGuid;
@end
