//
//  Task.m
//  iNappkinDesigner
//
//  Created by Safetli, Hazem on 16/10/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "Task.h"
#import "Project.h"

@implementation Task

@dynamic taskDescription;
@dynamic taskName;
@dynamic projectGuid;
@dynamic sync_status;
@dynamic is_deleted;
@dynamic guid;
@dynamic last_modified;
@dynamic destinationSketchGuid;
@dynamic firstSketchGuid;

@end
