//
//  Task.h
//  iNappkinDesigner
//
//  Created by Safetli, Hazem on 16/10/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "Project.h"
#import "Sketch.h"
#import "TapEvent.h"
#import "TaskStat.h"
@interface Task : NSManagedObject

@property (nonatomic, retain) NSString * taskDescription;
@property (nonatomic, retain) NSString * taskName;
@property (nonatomic, retain) NSString *projectGuid;
@property (nonatomic, retain) NSString *firstSketchGuid;
@property (nonatomic, retain) NSString *destinationSketchGuid;
@property (nonatomic, retain) NSString * sync_status;
@property (nonatomic, retain) NSString * is_deleted;
@property (nonatomic, retain) NSString * guid;
@property (nonatomic, retain) NSDate * last_modified;

@end
