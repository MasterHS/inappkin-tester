//
//  User.h
//  iNappkinTester
//
//  Created by Safetli, Hazem on 05/11/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
@class Project;

@interface User : NSManagedObject

@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSString * sync_status;
@property (nonatomic, retain) NSString * is_deleted;
@property (nonatomic, retain) NSString* guid;
@property (nonatomic, retain) NSString * last_modified;
@end
