//
//  Sketch.h
//  iNappkinDesigner
//
//  Created by Julian-Lennart Weigand on 22.08.14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Project, TransitionButton;

@interface Sketch : NSManagedObject

@property (nonatomic, retain) NSData * imageFile;
@property (nonatomic, retain) NSNumber * sketchOrderNumber;
@property (nonatomic, retain) NSString * projectGuid;
@property (nonatomic, retain) NSString * sync_status;
@property (nonatomic, retain) NSString * is_deleted;
@property (nonatomic, retain) NSString * guid;
@property (nonatomic, retain) NSDate * last_modified;
@property (nonatomic, retain) NSString * uploaded_new;
@end
