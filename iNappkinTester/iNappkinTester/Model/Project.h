//
//  Project.h
//  iNappkinDesigner
//
//  Created by Julian-Lennart Weigand on 20.08.14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Sketch,Task;


@interface Project : NSManagedObject

@property (nonatomic, retain) NSString * deviceType;
@property (nonatomic, retain) NSString * projectName;
@property (nonatomic, retain) NSString * projectDescription;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSString * sync_status;
@property (nonatomic, retain) NSString * is_deleted;
@property (nonatomic, retain) NSString * guid;
@property (nonatomic, retain) NSDate * last_modified;
@property (nonatomic, retain) NSString * deviceId;
@end

@interface Project (CoreDataGeneratedAccessors)

@end

