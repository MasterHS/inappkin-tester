//
//  TapEvent.h
//  iNappkinTester
//
//  Created by Safetli, Hazem on 27/10/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Sketch,Task;

@interface TapEvent : NSManagedObject

@property (nonatomic, retain) NSString * timeOnSketch;
@property (nonatomic, retain) NSNumber * xAxis;
@property (nonatomic, retain) NSNumber * yAxis;
@property (nonatomic, retain) NSString *taskGuid;
@property (nonatomic, retain) NSString *sketchGuid;
@property (nonatomic, retain) NSString * sync_status;
@property (nonatomic, retain) NSString * is_deleted;
@property (nonatomic, retain) NSDate * last_modified;
@property (nonatomic, retain) NSString * guid;
@property (nonatomic, retain) NSString * deviceType;

@end
