//
//  User.m
//  iNappkinTester
//
//  Created by Safetli, Hazem on 05/11/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "User.h"
#import "Project.h"

@implementation User

@dynamic password;
@dynamic userName;
@dynamic sync_status;
@dynamic is_deleted;
@dynamic guid;
@dynamic last_modified;
@end
