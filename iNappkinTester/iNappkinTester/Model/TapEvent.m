//
//  TapEvent.m
//  iNappkinTester
//
//  Created by Safetli, Hazem on 27/10/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "TapEvent.h"
#import "Sketch.h"


@implementation TapEvent

@dynamic timeOnSketch;
@dynamic xAxis;
@dynamic yAxis;
@dynamic taskGuid;
@dynamic sketchGuid;
@dynamic sync_status;
@dynamic is_deleted;
@dynamic guid;
@dynamic last_modified;
@dynamic deviceType;
@end
