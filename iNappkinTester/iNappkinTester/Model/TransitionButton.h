//
//  TransitionButton.h
//  iNappkinDesigner
//
//  Created by Julian-Lennart Weigand on 20.08.14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Sketch;

@interface TransitionButton : NSManagedObject

@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) NSNumber * width;
@property (nonatomic, retain) NSNumber * xAxis;
@property (nonatomic, retain) NSNumber * yAxis;
@property (nonatomic, retain) NSString *destinationSketchGuid;
@property (nonatomic, strong) NSString *sketchGuid;
@property (nonatomic, retain) NSString * sync_status;
@property (nonatomic, retain) NSString * is_deleted;
@property (nonatomic, retain) NSDate * last_modified;
@property (nonatomic, retain) NSString * guid;

@end
