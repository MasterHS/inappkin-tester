//
//  CoreDataManager.h
//  didacta
//
//  Created by Avr Messe on 21.06.10.
//  Copyright 2010 AVR GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Project, Sketch,TransitionButton;
#import "Task.h"

#import "User.h"
@interface CoreDataManager : NSObject {
    
    /*
     Class for data management
     */
    
@private
    NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
}

@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (CoreDataManager *)sharedManager;

-(id)getMaxLastModifiedinTable:(NSString*)tableName;

//Create, Update or Delete Core Data Objects
-(Project*)createOrModifyProject:(NSDictionary*)projectDict projectGuid:(NSString*)projGuid syncStatus:(NSString*)syncStatus deleted:(NSString*)deleted;
-(Sketch*)createOrModifySketch:(NSDictionary*)sketchDict sketchGuid:(NSString*)sketchGuid projectGuid:(NSString*)projGuid syncStatus:(NSString*)syncStatus deleted:(NSString*)deleted;
-(TransitionButton*)createOrModifyTransitionButton:(NSDictionary*)transDict transButtonGuid:(NSString*)transButtonGuid sketchGuid:(NSString*)sketchGuid syncStatus:(NSString*)syncStatus deleted:(NSString*)deleted;
-(Task*)createOrModifyTask:(NSDictionary*)dataDict taskGuid:(NSString*)taskGuid projectGuid:(NSString*)projGuid syncStatus:(NSString*)syncStatus deleted:(NSString*)deleted;
-(TapEvent*)createOrModifyTapEvent:(NSDictionary*)tapDict tapGuid:(NSString*)tapGuid sketchGuid:(NSString*)sketchGuid taskGuid:(NSString*)taskGuid syncStatus:(NSString*)syncStatus deleted:(NSString*)deleted;
-(TaskStat*)createOrModifyTaskStat:(NSDictionary*)statDict statGuid:(NSString*)statGuid taskGuid:(NSString*)taskGuid syncStatus:(NSString*)syncStatus deleted:(NSString*)deleted;


//Get Data Objects
-(NSArray*)DataToSync:(NSString*)tableName;
-(id)getDataById:(NSString*)guid inTable:(NSString*)tableName;
-(NSArray*)getAvailableProjects;
-(NSArray*)getTasks:(NSString*)projectGuid;
-(NSArray*)getSketches:(NSString*)projectGuid;
-(NSArray*)getTransitionButtons:(NSString*)sketchGuid;
-(NSArray*)getTapEvents:(NSString*)taskGuid sketchGuid:(NSString*)sketchGuid;
-(NSArray*)getTapEventsByTaskId:(NSString*)taskGuid;
-(NSArray*)getTaskStats:(NSString*)taskGuid;

//User
-(NSString*)addUser:(NSString*)userName password:(NSString*)pass withId:(NSString*)ID;
-(void)deleteUser:(NSString*)userName;
-(NSString*)getAvailableUser;

//-(void)resetDatastore;


@end
