//
//  TaskStat.h
//  iNappkinTester
//
//  Created by Safetli, Hazem on 05/11/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Task;

@interface TaskStat : NSManagedObject

@property (nonatomic, assign) NSString* taskCompletionTime;
@property (nonatomic, retain) NSString * numberOfMistakes;
@property (nonatomic, retain) NSString *taskGuid;
@property (nonatomic, retain) NSString * sync_status;
@property (nonatomic, retain) NSString * is_deleted;
@property (nonatomic, retain) NSString* guid;
@property (nonatomic, retain) NSString* deviceIdentifier;
@property (nonatomic, retain) NSString* timeToLearn;
@property (nonatomic, retain) NSString* taskCompleted;
@property (nonatomic, retain) NSString* timeSpentOnErrors;
@property (nonatomic, retain) NSDate * last_modified;
@end
