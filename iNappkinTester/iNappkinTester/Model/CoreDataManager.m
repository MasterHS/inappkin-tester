//
//  CoreDataManager.m
//
//  Created by Avr Messe on 21.06.10.
//  Copyright 2010 AVR GmbH. All rights reserved.
//

#import "CoreDataManager.h"
#import <CoreData/CoreData.h>
#import "Sketch.h"
#import "Project.h"
#import "TransitionButton.h"
#import "Task.h"
#import "NTHelpers.h"

@interface CoreDataManager ()

@property (nonatomic, retain) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end


@implementation CoreDataManager

@synthesize managedObjectModel;
@synthesize managedObjectContext;
@synthesize persistentStoreCoordinator;


static CoreDataManager *sharedDataManager = nil;


+(CoreDataManager *)sharedManager{
    @synchronized(self) {
        if (sharedDataManager == nil) {
            sharedDataManager= [[self alloc] init];
        }
    }
    return sharedDataManager;
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self) {
        if (sharedDataManager == nil) {
            sharedDataManager = [super allocWithZone:zone];
            return sharedDataManager;  // assignment and return on first allocation
        }
    }
    return nil; //on subsequent allocation attempts return nil
}

- (id)copyWithZone:(NSZone *)zone{
    return self;
}

# pragma mark - Persistierungsoperationen, managedObjectContext und managedObjectModel

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *) managedObjectContext {
    
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    return managedObjectContext;
}


/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created by merging all of the models found in the application bundle.
 */
- (NSManagedObjectModel *)managedObjectModel {
    
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    //managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil] ;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"CoreDataModel" ofType:@"momd"];
    NSURL *momURL = [NSURL fileURLWithPath:path];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:momURL];
    
    return managedObjectModel;
}


/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    
    NSURL *storeUrl = [NSURL fileURLWithPath: [basePath stringByAppendingPathComponent: @"iNappkin.sqlite"]];
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error]) {
        NSLog(@"NO persistent store Coordinator created");
    }
    
    // database should not be backed up on iCloud
    [self addSkipBackupAttributeToItemAtURL:storeUrl];
    
    return persistentStoreCoordinator;
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}


- (NSError *)saveData{
    NSError *err = nil;
    
    [self.managedObjectContext save:&err];
    return err;
}

//- (void)resetDatastore{
//    //Keep the available user beside
//
//    NSError* error;
//    NSManagedObjectContext *mOC = [self managedObjectContext];
//    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
//    [request setPredicate:[NSPredicate predicateWithFormat:nil]];
//    [request setResultType:NSDictionaryResultType];
//    NSDictionary * userDict = [mOC executeFetchRequest:request error:&error][0];
//    [[self managedObjectContext] lock];
//    [[self managedObjectContext] reset];
//    NSPersistentStore *store = [[[self persistentStoreCoordinator] persistentStores] lastObject];
//    BOOL resetOk = NO;
//    if (store){
//        NSURL *storeUrl = store.URL;
//        NSError *error;
//
//        if ([[self persistentStoreCoordinator] removePersistentStore:store error:&error]){
//            persistentStoreCoordinator = nil;
//            managedObjectContext = nil;
//
//            if (![[NSFileManager defaultManager] removeItemAtPath:storeUrl.path error:&error]) {
//                NSLog(@"\nresetDatastore. Error removing file of persistent store: %@",
//                      [error localizedDescription]);
//                resetOk = NO;
//            }
//            else{
//                //now recreate persistent store
//                [self persistentStoreCoordinator];
//                [[self managedObjectContext] unlock];
//                resetOk = YES;
//                NSManagedObjectContext *mOC = [self managedObjectContext];
//                User *user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:mOC];
//                user.password=[userDict objectForKey:@"password"];
//                user.userName=[userDict objectForKey:@"userName"];
//                user.guid=[userDict objectForKey:@"userId"];
//                [self.managedObjectContext save:nil];
//            }
//        }else{
//            NSLog(@"\nresetDatastore. Error removing persistent store: %@",
//                  [error localizedDescription]);
//            resetOk = NO;
//        }
//    }else{
//        NSLog(@"\nresetDatastore. Could not find the persistent store");
//    }
//}



# pragma mark Create, Update or Delete Core Data Objects

-(Project*)createOrModifyProject:(NSDictionary*)projectDict projectGuid:(NSString*)projGuid syncStatus:(NSString*)syncStatus deleted:(NSString*)deleted
{
    Project *project=nil;
    if (projGuid!=nil) {
        project=[self getDataById:projGuid inTable:@"Project"];
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    NSLocale* posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.locale = posix;
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"CET"]];
    //if count=0 it means two cases:
    //1. the project is created on the current user side
    //2. project is created by another user, so it's downloaded from the server and created on the currnet user side.
    if(project == nil)
    {
        NSManagedObjectContext *mOC = [self managedObjectContext];
        project = [NSEntityDescription insertNewObjectForEntityForName:@"Project" inManagedObjectContext:mOC];
        project.projectName = [projectDict objectForKey:@"projectName"];
        project.projectDescription = [projectDict objectForKey:@"projectDescription"];
        //if sync status ==1 =>project is created locally and device Id must be taken from the current device
        //else if sync status =0 => project is synced from server and dev Id is already attached with project object
        if([syncStatus isEqualToString: @"1"])
            project.deviceId=[NTHelpers getDevId];
        else
            project.deviceId=[projectDict objectForKey:@"deviceId"];
        
        project.deviceType = [projectDict objectForKey:@"deviceType"];
        project.userName = [projectDict objectForKey:@"userName"];
        //if last_modified wasn't included in the dict. => the project is completetly new and it has never been on the server before
        //else last_modified has value=> take it as it is
        if(![projectDict objectForKey:@"last_modified"])
            project.last_modified =nil;
        else
            project.last_modified =[dateFormatter dateFromString:[projectDict objectForKey:@"last_modified"]];
        //if guid is not listed in the dict.=> the project is completly new and needs to generate a new GUID for it. Otherwise, take the provided guid
        if(projGuid==nil)
        {
            project.guid=[[NSUUID UUID] UUIDString];
            //project is created on the current user side, and needs to be synced to the server
            project.sync_status=@"1";
        }
        else
        {
            project.guid=[projectDict objectForKey:@"guid"];
            //project downloaded from server and there is no need for setting the sync flag
            project.sync_status=@"0";
        }
        
        project.is_deleted=deleted;
        if(![deleted isEqualToString: @"0"])
        {
            [self deleteProject:project];
        }
    }
    else//modify project
    {
        //sync flag is always 1 since we are modifying the project if count==1
        project.sync_status=syncStatus;
        if([projectDict objectForKey:@"projectName"])
            project.projectName = [projectDict objectForKey:@"projectName"];
        if([projectDict objectForKey:@"projectDescription"])
            project.projectDescription = [projectDict objectForKey:@"projectDescription"];
        if([projectDict objectForKey:@"deviceType"])
            project.deviceType = [projectDict objectForKey:@"deviceType"];
        if([projectDict objectForKey:@"userName"])
            project.userName = [projectDict objectForKey:@"userName"];
        if([projectDict objectForKey:@"last_modified"])
            project.last_modified = [dateFormatter dateFromString: [projectDict objectForKey:@"last_modified"]];
        project.is_deleted=deleted;
        //if the project is only create on the client side and has not been synced yet -> delete it permenantly
        if([deleted isEqualToString:@"1"] && project.last_modified == nil)
            project.is_deleted=@"3";
        if(![deleted isEqualToString: @"0"])
        {
            [self deleteProject:project];
        }
        
    }
    [self.managedObjectContext save:nil];
    return project;
}

-(Sketch*)createOrModifySketch:(NSDictionary*)sketchDict sketchGuid:(NSString*)sketchGuid projectGuid:(NSString*)projGuid syncStatus:(NSString*)syncStatus deleted:(NSString*)deleted
{
    Sketch *sketch=nil;
    if (sketchGuid!=nil) {
        sketch=[self getDataById:sketchGuid inTable:@"Sketch"];
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    NSLocale* posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.locale = posix;
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"CET"]];
    //if count=0 it means two cases:
    //1. the sketch is created on the current user side
    //2. project is created by another user, so it's downloaded from the server and created on the currnet user side.
    if(sketch==nil)
    {
        
        NSManagedObjectContext *mOC = [self managedObjectContext];
        sketch = [NSEntityDescription insertNewObjectForEntityForName:@"Sketch" inManagedObjectContext:mOC];
        sketch.sync_status=syncStatus;
        if([sketchDict objectForKey:@"imageFile"])
            sketch.imageFile  = [[NSData alloc] initWithBase64EncodedString:[sketchDict objectForKey:@"imageFile"] options:0];
        else
            sketch.imageFile=[sketchDict objectForKey:@"imageFile"];
        
        if([sketchDict objectForKey:@"sketchOrderNumber"])
            sketch.sketchOrderNumber=@([[sketchDict objectForKey:@"sketchOrderNumber"] intValue]);
        else
        {
            NSNumber *sketchesCount = [NSNumber numberWithInteger:[[self getSketches:projGuid]count]];
            sketch.sketchOrderNumber=sketchesCount;
        }
        sketch.projectGuid=projGuid;
        
        //if last_modified wasn't included in the dict. => the sketch is completetly new and it has never been on the server before
        //else last_modified has value=> take it as it is
        if(![sketchDict objectForKey:@"last_modified"])
            sketch.last_modified =nil;
        else
            sketch.last_modified =[dateFormatter dateFromString:[sketchDict objectForKey:@"last_modified"]];
        //if guid is not listed in the dict.=> the project is completly new and needs to generate a new GUID for it. Otherwise, take the provided guid
        if(sketchGuid ==nil)
        {
            sketch.guid=[[NSUUID UUID] UUIDString];
            sketch.uploaded_new=@"1";
        }
        else
        {
            sketch.guid=[sketchDict objectForKey:@"guid"];
            sketch.uploaded_new=@"0";
        }
        sketch.is_deleted=deleted;
        if(![deleted isEqualToString: @"0"])
        {
            [self deleteSketch:sketch];
        }
    }
    else//modify project
    {
        sketch.sync_status=syncStatus;
        if([sketchDict objectForKey:@"sketchOrderNumber"])
            sketch.sketchOrderNumber = @([[sketchDict objectForKey:@"sketchOrderNumber"] intValue]);
        //        if([sketchDict objectForKey:@"imageFile"])
        //            sketch.imageFile = [sketchDict objectForKey:@"imageFile"];
        if([sketchDict objectForKey:@"last_modified"])
            sketch.last_modified =[dateFormatter dateFromString: [sketchDict objectForKey:@"last_modified"]];
        sketch.projectGuid=projGuid;
        sketch.is_deleted=deleted;
        if([deleted isEqualToString:@"1"] && sketch.last_modified == nil)
            sketch.is_deleted=@"3";
        if(![deleted isEqualToString: @"0"])
        {
            [self deleteSketch:sketch];
        }
    }
    [self.managedObjectContext save:nil];
    return sketch;
}

-(Task*)createOrModifyTask:(NSDictionary*)dataDict taskGuid:(NSString*)taskGuid projectGuid:(NSString*)projGuid syncStatus:(NSString*)syncStatus deleted:(NSString*)deleted
{
    Task *task=nil;
    if (taskGuid!=nil) {
        task=[self getDataById:taskGuid inTable:@"Task"];
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    NSLocale* posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.locale = posix;
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"CET"]];
    //if count=0 it means two cases:
    //1. the sketch is created on the current user side
    //2. project is created by another user, so it's downloaded from the server and created on the currnet user side.
    if(task==nil)
    {
        
        NSManagedObjectContext *mOC = [self managedObjectContext];
        task = [NSEntityDescription insertNewObjectForEntityForName:@"Task" inManagedObjectContext:mOC];
        task.sync_status=syncStatus;
        task.projectGuid=projGuid;
        task.taskName=[dataDict objectForKey:@"taskName"];
        task.taskDescription=[dataDict objectForKey:@"taskDescription"];
        task.firstSketchGuid=[dataDict objectForKey:@"firstSketchGuid"];
        task.destinationSketchGuid=[dataDict objectForKey:@"destinationSketchGuid"];
        
        if(![dataDict objectForKey:@"last_modified"])
            task.last_modified =nil;
        else
            task.last_modified =[dateFormatter dateFromString:[dataDict objectForKey:@"last_modified"]];
        //if guid is not listed in the dict.=> the project is completly new and needs to generate a new GUID for it. Otherwise, take the provided guid
        if(taskGuid ==nil)
        {
            task.guid=[[NSUUID UUID] UUIDString];
        }
        else
        {
            task.guid=[dataDict objectForKey:@"guid"];
        }
        task.is_deleted=deleted;
        if(![deleted isEqualToString: @"0"])
        {
            [self deleteTask:task];
        }
    }
    else//modify project
    {
        task.sync_status=syncStatus;
        if([dataDict objectForKey:@"taskName"])
            task.taskName=[dataDict objectForKey:@"taskName"];
        if([dataDict objectForKey:@"taskDescription"])
            task.taskDescription=[dataDict objectForKey:@"taskDescription"];
        if([dataDict objectForKey:@"firstSketchGuid"])
            task.firstSketchGuid=[dataDict objectForKey:@"firstSketchGuid"];
        if([dataDict objectForKey:@"destinationSketchGuid"])
            task.destinationSketchGuid=[dataDict objectForKey:@"destinationSketchGuid"];
        
        if([dataDict objectForKey:@"last_modified"])
            task.last_modified =[dateFormatter dateFromString: [dataDict objectForKey:@"last_modified"]];
        task.projectGuid=projGuid;
        task.is_deleted=deleted;
        if([deleted isEqualToString:@"1"] && task.last_modified == nil)
            task.is_deleted=@"3";
        if(![deleted isEqualToString: @"0"])
        {
            [self deleteTask:task];
        }
    }
    [self.managedObjectContext save:nil];
    return task;
}

-(TransitionButton*)createOrModifyTransitionButton:(NSDictionary*)transDict transButtonGuid:(NSString*)transButtonGuid sketchGuid:(NSString*)sketchGuid syncStatus:(NSString*)syncStatus deleted:(NSString*)deleted
{
    TransitionButton *transButton=nil;
    if (transButtonGuid!=nil) {
        transButton=[self getDataById:transButtonGuid inTable:@"TransitionButton"];
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    NSLocale* posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.locale = posix;
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"CET"]];
    
    //if count=0 it means two cases:
    //1. the project is created on the current user side
    //2. project is created by another user, so it's downloaded from the server and created on the currnet user side.
    if(transButton == nil)
    {
        NSManagedObjectContext *mOC = [self managedObjectContext];
        transButton = [NSEntityDescription insertNewObjectForEntityForName:@"TransitionButton" inManagedObjectContext:mOC];
        transButton.sync_status=syncStatus;
        transButton.xAxis=  [NSNumber numberWithFloat: [[transDict objectForKey:@"xAxis"]floatValue]];
        transButton.yAxis= [NSNumber numberWithFloat:[[transDict objectForKey:@"yAxis"]floatValue]];
        transButton.width= [NSNumber numberWithFloat: [[transDict objectForKey:@"width"]floatValue]];
        transButton.height= [NSNumber numberWithFloat: [[transDict objectForKey:@"height"]floatValue]];
        transButton.sketchGuid=sketchGuid;
        transButton.destinationSketchGuid=[transDict objectForKey:@"destinationSketchGuid"];
        
        if(![transDict objectForKey:@"last_modified"])
            transButton.last_modified =nil;
        else
            transButton.last_modified =[dateFormatter dateFromString:[transDict objectForKey:@"last_modified"]];
        //if guid is not listed in the dict.=> the project is completly new and needs to generate a new GUID for it. Otherwise, take the provided guid
        if(transButtonGuid==nil)
        {
            transButton.guid=[[NSUUID UUID] UUIDString];
        }
        else
        {
            transButton.guid=[transDict objectForKey:@"guid"];
        }
        transButton.is_deleted=deleted;
        if(![deleted isEqualToString: @"0"])
        {
            [self deleteTransitionButton:transButton];
        }
    }
    else//modify project
    {
        transButton.sync_status=syncStatus;
        if([transDict objectForKey:@"xAxis"])
            transButton.xAxis=  [NSNumber numberWithFloat: [[transDict objectForKey:@"xAxis"]floatValue]];
        if([transDict objectForKey:@"yAxis"])
            transButton.yAxis= [NSNumber numberWithFloat:[[transDict objectForKey:@"yAxis"]floatValue]];
        if([transDict objectForKey:@"width"])
            transButton.width= [NSNumber numberWithFloat: [[transDict objectForKey:@"width"]floatValue]];
        if([transDict objectForKey:@"height"])
            transButton.height= [NSNumber numberWithFloat: [[transDict objectForKey:@"height"]floatValue]];
        if([transDict objectForKey:@"destinationSketchGuid"])
            transButton.destinationSketchGuid=[transDict objectForKey:@"destinationSketchGuid"];
        if([transDict objectForKey:@"last_modified"])
            transButton.last_modified = [dateFormatter dateFromString: [transDict objectForKey:@"last_modified"]];
        transButton.is_deleted=deleted;
        transButton.sketchGuid=sketchGuid;
        if([deleted isEqualToString:@"1"] && transButton.last_modified == nil)
            transButton.is_deleted=@"3";
        if(![deleted isEqualToString: @"0"])
        {
            [self deleteTransitionButton:transButton];
        }
        
    }
    [self.managedObjectContext save:nil];
    return transButton;
}

-(TapEvent*)createOrModifyTapEvent:(NSDictionary*)tapDict tapGuid:(NSString*)tapGuid sketchGuid:(NSString*)sketchGuid taskGuid:(NSString*)taskGuid syncStatus:(NSString*)syncStatus deleted:(NSString*)deleted
{
    TapEvent *tapEvent=nil;
    if (tapGuid!=nil) {
        tapEvent=[self getDataById:tapGuid inTable:@"TapEvent"];
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    NSLocale* posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.locale = posix;
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"CET"]];
    
    //if count=0 it means two cases:
    //1. the project is created on the current user side
    //2. project is created by another user, so it's downloaded from the server and created on the currnet user side.
    if(tapEvent == nil)
    {
        NSManagedObjectContext *mOC = [self managedObjectContext];
        tapEvent = [NSEntityDescription insertNewObjectForEntityForName:@"TapEvent" inManagedObjectContext:mOC];
        tapEvent.sync_status=syncStatus;
        tapEvent.xAxis=  [NSNumber numberWithFloat: [[tapDict objectForKey:@"xAxis"]floatValue]];
        tapEvent.yAxis= [NSNumber numberWithFloat:[[tapDict objectForKey:@"yAxis"]floatValue]];
        tapEvent.timeOnSketch=[tapDict objectForKey:@"timeOnSketch"];
        tapEvent.sketchGuid=sketchGuid;
        tapEvent.taskGuid=taskGuid;
        tapEvent.deviceType=[tapDict objectForKey:@"deviceType"];
        
        if(![tapDict objectForKey:@"last_modified"])
            tapEvent.last_modified =nil;
        else
            tapEvent.last_modified =[dateFormatter dateFromString:[tapDict objectForKey:@"last_modified"]];
        //if guid is not listed in the dict.=> the project is completly new and needs to generate a new GUID for it. Otherwise, take the provided guid
        if(tapGuid==nil)
        {
            tapEvent.guid=[[NSUUID UUID] UUIDString];
        }
        else
        {
            tapEvent.guid=[tapDict objectForKey:@"guid"];
        }
        tapEvent.is_deleted=deleted;
        if(![deleted isEqualToString: @"0"])
        {
            [self deleteTapEvent:tapEvent];
        }
    }
    else//modify project
    {
        tapEvent.sync_status=syncStatus;
        if([tapDict objectForKey:@"xAxis"])
            tapEvent.xAxis=  [NSNumber numberWithFloat: [[tapDict objectForKey:@"xAxis"]floatValue]];
        if([tapDict objectForKey:@"yAxis"])
            tapEvent.yAxis= [NSNumber numberWithFloat:[[tapDict objectForKey:@"yAxis"]floatValue]];
        if([tapDict objectForKey:@"timeOnSketch"])
            tapEvent.timeOnSketch=[tapDict objectForKey:@"timeOnSketch"];
        if([tapDict objectForKey:@"deviceType"])
            tapEvent.deviceType=[tapDict objectForKey:@"deviceType"];
        if([tapDict objectForKey:@"last_modified"])
            tapEvent.last_modified = [dateFormatter dateFromString: [tapDict objectForKey:@"last_modified"]];
        tapEvent.is_deleted=deleted;
        tapEvent.sketchGuid=sketchGuid;
        tapEvent.taskGuid=taskGuid;
        if([deleted isEqualToString:@"1"] && tapEvent.last_modified == nil)
            tapEvent.is_deleted=@"3";
        if(![deleted isEqualToString: @"0"])
        {
            [self deleteTapEvent:tapEvent];
        }
        
    }
    [self.managedObjectContext save:nil];
    return tapEvent;
}

-(TaskStat*)createOrModifyTaskStat:(NSDictionary*)statDict statGuid:(NSString*)statGuid taskGuid:(NSString*)taskGuid syncStatus:(NSString*)syncStatus deleted:(NSString*)deleted
{
    TaskStat *taskStat=nil;
    if (statGuid!=nil) {
        taskStat=[self getDataById:statGuid inTable:@"TaskStat"];
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    NSLocale* posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.locale = posix;
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"CET"]];
    
    //if count=0 it means two cases:
    //1. the project is created on the current user side
    //2. project is created by another user, so it's downloaded from the server and created on the currnet user side.
    if(taskStat == nil)
    {
        NSManagedObjectContext *mOC = [self managedObjectContext];
        taskStat = [NSEntityDescription insertNewObjectForEntityForName:@"TaskStat" inManagedObjectContext:mOC];
        taskStat.sync_status=syncStatus;
        
        taskStat.taskGuid=taskGuid;
        taskStat.taskCompletionTime=[statDict objectForKey:@"taskCompletionTime"];
        taskStat.numberOfMistakes=[statDict objectForKey:@"numberOfMistakes"];
        taskStat.timeToLearn=[statDict objectForKey:@"timeToLearn"];
        taskStat.taskCompleted=[statDict objectForKey:@"taskCompleted"];
        taskStat.timeSpentOnErrors=[statDict objectForKey:@"timeSpentOnErrors"];
        taskStat.deviceIdentifier=[statDict objectForKey:@"deviceIdentifier"];
        
        
        if(![statDict objectForKey:@"last_modified"])
            taskStat.last_modified =nil;
        else
            taskStat.last_modified =[dateFormatter dateFromString:[statDict objectForKey:@"last_modified"]];
        //if guid is not listed in the dict.=> the project is completly new and needs to generate a new GUID for it. Otherwise, take the provided guid
        if(statGuid==nil)
        {
            taskStat.guid=[[NSUUID UUID] UUIDString];
        }
        else
        {
            taskStat.guid=[statDict objectForKey:@"guid"];
        }
        taskStat.is_deleted=deleted;
        if(![deleted isEqualToString: @"0"])
        {
            [self deleteTaskStat:taskStat];
        }
    }
    else//modify project
    {
        taskStat.sync_status=syncStatus;
        if([statDict objectForKey:@"taskCompletionTime"])
            taskStat.taskCompletionTime=[statDict objectForKey:@"taskCompletionTime"];
        if([statDict objectForKey:@"numberOfMistakes"])
            taskStat.numberOfMistakes=[statDict objectForKey:@"numberOfMistakes"];
        if([statDict objectForKey:@"timeToLearn"])
            taskStat.timeToLearn=[statDict objectForKey:@"timeToLearn"];
        if([statDict objectForKey:@"taskCompleted"])
            taskStat.taskCompleted=[statDict objectForKey:@"taskCompleted"];
        if([statDict objectForKey:@"timeSpentOnErrors"])
            taskStat.timeSpentOnErrors=[statDict objectForKey:@"timeSpentOnErrors"];
        if([statDict objectForKey:@"deviceIdentifier"])
            taskStat.deviceIdentifier=[statDict objectForKey:@"deviceIdentifier"];
        
        
        
        if([statDict objectForKey:@"last_modified"])
            taskStat.last_modified = [dateFormatter dateFromString: [statDict objectForKey:@"last_modified"]];
        taskStat.is_deleted=deleted;
        taskStat.taskGuid=taskGuid;
        if([deleted isEqualToString:@"1"] && taskStat.last_modified == nil)
            taskStat.is_deleted=@"3";
        if(![deleted isEqualToString: @"0"])
        {
            [self deleteTaskStat:taskStat];
        }
        
    }
    [self.managedObjectContext save:nil];
    return taskStat;
}

#pragma mark Get Objects

-(NSArray*)DataToSync:(NSString*)tableName
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:tableName];
    [request setResultType:NSDictionaryResultType];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sync_status == 1"];
    [request setPredicate:predicate];
    
    NSArray *data = [self.managedObjectContext executeFetchRequest:request error:nil];
    return data;
}

-(id)getMaxLastModifiedinTable:(NSString*)tableName
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:tableName];
    request.fetchLimit = 1;
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"last_modified" ascending:NO]];
    
    NSArray *data = [self.managedObjectContext executeFetchRequest:request error:nil];
    
    if([data count]==0)
        return nil;
    else
        return data[0];
}


-(id)getDataById:(NSString*)guid inTable:(NSString*)tableName
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:tableName];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"guid == %@",guid];
    [request setPredicate:predicate];
    NSArray *data = [self.managedObjectContext executeFetchRequest:request error:nil];
    if([data count]==0)
        return nil;
    else
        return data[0];
}

-(NSArray*)getAvailableProjects
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Project"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"is_deleted == 0"];
    [request setPredicate:predicate];
    NSArray *projects = [self.managedObjectContext executeFetchRequest:request error:nil];
    return projects;
}

-(NSArray*)getSketches:(NSString*)projectGuid
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Sketch"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"projectGuid == %@ AND is_deleted == 0",projectGuid];
    [request setPredicate:predicate];
    NSArray *data = [self.managedObjectContext executeFetchRequest:request error:nil];
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sketchOrderNumber"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [data sortedArrayUsingDescriptors:sortDescriptors];
    if([sortedArray count]==0)
        return nil;
    else
        return sortedArray;
}

-(NSArray*)getTasks:(NSString*)projectGuid
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Task"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"projectGuid == %@ AND is_deleted == 0",projectGuid];
    [request setPredicate:predicate];
    NSArray *data = [self.managedObjectContext executeFetchRequest:request error:nil];
    if([data count]==0)
        return nil;
    else
        return data;
}

-(NSArray*)getTransitionButtons:(NSString*)sketchGuid
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"TransitionButton"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sketchGuid == %@ AND is_deleted == 0",sketchGuid];
    [request setPredicate:predicate];
    NSArray *data = [self.managedObjectContext executeFetchRequest:request error:nil];
    if([data count]==0)
        return nil;
    else
        return data;
}

-(NSArray*)getTapEvents:(NSString*)taskGuid sketchGuid:(NSString*)sketchGuid
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"TapEvent"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sketchGuid == %@ AND taskGuid == %@ AND is_deleted == 0",sketchGuid,taskGuid];
    [request setPredicate:predicate];
    NSArray *data = [self.managedObjectContext executeFetchRequest:request error:nil];
    if([data count]==0)
        return nil;
    else
        return data;
}

-(NSArray*)getTapEventsByTaskId:(NSString*)taskGuid
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"TapEvent"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"taskGuid == %@ AND is_deleted == 0",taskGuid];
    [request setPredicate:predicate];
    NSArray *data = [self.managedObjectContext executeFetchRequest:request error:nil];
    if([data count]==0)
        return nil;
    else
        return data;
}

-(NSArray*)getTaskStats:(NSString*)taskGuid
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"TaskStat"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"taskGuid == %@ AND is_deleted == 0",taskGuid];
    [request setPredicate:predicate];
    NSArray *data = [self.managedObjectContext executeFetchRequest:request error:nil];
    if([data count]==0)
        return nil;
    else
        return data;
}

#pragma mark Delete Objects

-(void)deleteProject:(Project*)project{
    NSArray* sketches=[[CoreDataManager sharedManager]getSketches:project.guid];
    NSArray* tasks=[[CoreDataManager sharedManager]getTasks:project.guid];
    
    if([project.is_deleted isEqualToString:@"1"])//delete
    {
        project.sync_status=@"1";
        if([sketches count])
        {
            for(Sketch* sketch in sketches)
            {
                sketch.is_deleted=project.is_deleted;
                [self deleteSketch:sketch];
            }
        }
        if([tasks count])
        {
            for(Task* task in tasks)
            {
                task.is_deleted=project.is_deleted;
                [self deleteTask:task];
            }
        }
    }
    else if([project.is_deleted isEqualToString:@"2"])// object is delted on the server and must be deleted locally (permenatly)
    {
        [self.managedObjectContext deleteObject:project];
    }
    else if([project.is_deleted isEqualToString:@"3"])//Object is not synced yet+ is deleted -> delete it permenantly
    {
        project.sync_status=@"0";
        if([sketches count])
        {
            for(Sketch* sketch in sketches)
            {
                sketch.is_deleted=project.is_deleted;
                [self deleteSketch:sketch];
            }
        }
        if([tasks count])
        {
            for(Task* task in tasks)
            {
                task.is_deleted=project.is_deleted;
                [self deleteTask:task];
            }
        }
        [self.managedObjectContext deleteObject:project];
    }
    [self.managedObjectContext save:nil];
}

-(void)deleteSketch:(Sketch*)sketch{
    sketch.imageFile=nil;
    NSArray* transitionButtons=[[CoreDataManager sharedManager]getTransitionButtons:sketch.guid];
    if([sketch.is_deleted isEqualToString:@"1"])
    {
        sketch.sync_status=@"1";
        if([transitionButtons count])
        {
            for(TransitionButton* transButton in transitionButtons)
            {
                transButton.is_deleted=sketch.is_deleted;
                [self deleteTransitionButton:transButton];
            }
        }
    }
    else if([sketch.is_deleted isEqualToString:@"2"])//delete permenatly
    {
        [self.managedObjectContext deleteObject:sketch];
    }
    else if([sketch.is_deleted isEqualToString:@"3"])
    {
        sketch.sync_status=@"0";
        if([transitionButtons count])
        {
            for(TransitionButton* transButton in transitionButtons)
            {
                transButton.is_deleted=sketch.is_deleted;
                [self deleteTransitionButton:transButton];
            }
        }
        [self.managedObjectContext deleteObject:sketch];
    }
    
    [self.managedObjectContext save:nil];
}

-(void)deleteTask:(Task*)task{
    NSArray* taskStats=[self getTaskStats:task.guid];
    NSArray* tasks=[self getTapEventsByTaskId:task.guid];
    if([task.is_deleted isEqualToString:@"1"])
    {
        task.sync_status=@"1";
        if([tasks count])
        {
            for(Task* task in tasks)
            {
                task.is_deleted=task.is_deleted;
                [self deleteTask:task];
            }
        }
        if([taskStats count])
        {
            for(TaskStat* stat in taskStats)
            {
                stat.is_deleted=task.is_deleted;
                [self deleteTaskStat:stat];
            }
        }
    }
    else if([task.is_deleted isEqualToString:@"2"])//delete permenatly
    {
        [self.managedObjectContext deleteObject:task];
    }
    else if([task.is_deleted isEqualToString:@"3"])
    {
        task.sync_status=@"0";
        if([tasks count])
        {
            for(Task* task in tasks)
            {
                task.is_deleted=task.is_deleted;
                [self deleteTask:task];
            }
        }
        if([taskStats count])
        {
            for(TaskStat* stat in taskStats)
            {
                stat.is_deleted=task.is_deleted;
                [self deleteTaskStat:stat];
            }
        }
        [self.managedObjectContext deleteObject:task];
    }
    [self.managedObjectContext save:nil];
}

-(void)deleteTaskStat:(TaskStat*)taskStat{
    if([taskStat.is_deleted isEqualToString:@"1"])
    {
        taskStat.sync_status=@"1";
    }
    else if([taskStat.is_deleted isEqualToString:@"2"])//delete permenatly
    {
        [self.managedObjectContext deleteObject:taskStat];
    }
    else if([taskStat.is_deleted isEqualToString:@"3"])
    {
        taskStat.sync_status=@"0";
        [self.managedObjectContext deleteObject:taskStat];
    }
    [self.managedObjectContext save:nil];
}

-(void)deleteTapEvent:(TapEvent*)tapEvent{
    if([tapEvent.is_deleted isEqualToString:@"1"])
    {
        tapEvent.sync_status=@"1";
    }
    else if([tapEvent.is_deleted isEqualToString:@"2"])//delete permenatly
    {
        [self.managedObjectContext deleteObject:tapEvent];
    }
    else if([tapEvent.is_deleted isEqualToString:@"3"])
    {
        tapEvent.sync_status=@"0";
        [self.managedObjectContext deleteObject:tapEvent];
    }
    [self.managedObjectContext save:nil];
}

-(void)deleteTransitionButton:(TransitionButton*)button{
    if([button.is_deleted isEqualToString:@"1"])
    {
        button.sync_status=@"1";
    }
    else if([button.is_deleted isEqualToString:@"2"])//delete permenatly
    {
        [self.managedObjectContext deleteObject:button];
    }
    else if([button.is_deleted isEqualToString:@"3"])
    {
        button.sync_status=@"0";
        [self.managedObjectContext deleteObject:button];
    }
    
    [self.managedObjectContext save:nil];
}
#pragma mark User

-(NSString*)addUser:(NSString*)userName password:(NSString*)pass withId:(NSString*)ID
{
    
    NSError* error;
    NSManagedObjectContext *mOC = [self managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"userName = %@", userName]];
    
    //[request setFetchLimit:1];
    [request setResultType:NSDictionaryResultType];
    NSArray * users = [mOC executeFetchRequest:request error:&error];
    NSUInteger count = [mOC countForFetchRequest:request error:&error];
    
    if (count == NSNotFound)
    {
        
    }
    else if (count == 0)//user is new
    {
        [request setPredicate:[NSPredicate predicateWithFormat:nil]];
        [request setResultType:NSManagedObjectResultType];
        NSArray * allUsers = [mOC executeFetchRequest:request error:&error];
        //delete other users => logout them
        for (NSManagedObject * user in allUsers) {
            [mOC deleteObject:user];
        }
        [self.managedObjectContext save:nil];
        
        User *us = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:mOC];
        us.password=pass;
        us.userName=userName;
        us.guid=[[NSUUID UUID] UUIDString];
        [self.managedObjectContext save:nil];
        return us.userName;
    }
    else if(count==1)//user alread in database => login automatically
    {
        
        if (users == nil) {
            // Handle the error.
        }
        else{//we take index 0 becuase we myst always have 1 record in user database
            return [users[0] objectForKey:@"userName"];
        }
        
    }
    [self.managedObjectContext save:nil];
    return  nil;
}


-(void)deleteUser:(NSString*)userName
{
    NSError* error;
    NSManagedObjectContext *mOC = [self managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"userName = %@", userName]];
    
    //[request setFetchLimit:1];
    [request setResultType:NSManagedObjectResultType];
    NSArray * users = [mOC executeFetchRequest:request error:&error];
    //NSUInteger count = [mOC countForFetchRequest:request error:&error];
    [mOC deleteObject:users[0]];
    [self.managedObjectContext save:nil];
}

-(NSString*)getAvailableUser
{
    NSError* error;
    NSManagedObjectContext *mOC = [self managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription  entityForName:@"User" inManagedObjectContext:mOC];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    [request setResultType:NSDictionaryResultType];
    [request setReturnsDistinctResults:YES];
    
    NSUInteger count = [mOC countForFetchRequest:request error:&error];
    [request setResultType:NSDictionaryResultType];
    NSArray *users = [mOC executeFetchRequest:request error:&error];
    if(count==1)
    {
        return [users[0] objectForKey:@"userName"];;
    }
    [self.managedObjectContext save:nil];
    return @"";
    
}




@end
