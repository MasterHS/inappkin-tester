//
//  TaskStat.m
//  iNappkinTester
//
//  Created by Safetli, Hazem on 05/11/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "TaskStat.h"


@implementation TaskStat

@dynamic taskGuid;
@dynamic taskCompleted;
@dynamic taskCompletionTime;
@dynamic sync_status;
@dynamic is_deleted;
@dynamic timeSpentOnErrors;
@dynamic deviceIdentifier;
@dynamic last_modified;
@dynamic timeToLearn;
@dynamic numberOfMistakes;
@dynamic guid;
@end
