//
//  Sketch.m
//  iNappkinDesigner
//
//  Created by Julian-Lennart Weigand on 22.08.14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "Sketch.h"
#import "Project.h"
#import "TransitionButton.h"


@implementation Sketch

@dynamic imageFile;
@dynamic sketchOrderNumber;
@dynamic projectGuid;
@dynamic sync_status;
@dynamic is_deleted;
@dynamic last_modified;
@dynamic guid;
@dynamic uploaded_new;


@end
