//
//  NDHelpers.h
//  iNappkinDesigner
//
//  Created by Orest on 17/05/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Eine Helper Klasse, die kleine Funktionalität bereitstellen soll.
 Ich sehe nicht die Notwendigkeit die devID hier auszulesen oder die Alertview für die Anzeige hier zu generieren.
 Den ADSServerrequest Handler kann man auch diret vor Ort generieren (wird wohl eh nur 1 mal gemacht). Oder sollte das hier ein Singleton werden?
 
 ### Falls die Klasse umgebaut/geloescht wird die URL-Strings nicht vergessen.
 */

@import Foundation;
@class ADSProject;
@class ADSServerRequestHandler;
@class NTAppDelegate;

// Server URLs
extern NSString * const BaseURLString;
extern NSString * const BaseURLStringForImage;

NTAppDelegate * appDelegate;

@interface NTHelpers : NSObject

+ (void) initHelpers;

+(NSString*)getBaseServerName;
+(NSString*)getImageServerName;
+ (NSString*) deviceName
+ (NSString *) getDevId;

+ (UIColor *)normalTextBoarderColor;
+ (UIColor *)tasksBoarderColor;
+(UIColor *)blueButtonColor;
+(UIColor *)purpleButtonColor;

+(NSDictionary*) getAspectRatioAndScreensizeForDeviceType: (NSString*)deviceType;
+ (UIImage*)cropScaleAndCompressImage:(UIImage*)image forDeviceType:(NSString*)deviceType;

+(BOOL) checkNameValidityWithProjectName:(NSString*)projektName;

+(NSString*)getSyncStatus;
+(void)setSyncStatus:(NSString*)status;

@end
